#    jump_dive_clock -  Speedrun timer for Linux.
#    Copyright (C) 2023-2024  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
EXE_NAME = jump_dive_clock
INSTALL_FOLDER = "$(shell echo $$HOME)/.local/bin/"

DEBUG_ARGS = config_folder:$(shell pwd) split:example

EXTRA_FLAGS = "-d:GraphicsApiOpenGl11 --mm:orc --threads:on"
RELEASE_FLAGS = "--os:linux --opt:speed"

.PHONY: all debug-build release-build clean

# Default target
all: debug-build

debug-build:
	nimble build $(EXTRA_FLAGS) -d:debug $(EXE_NAME)

debug-build-wayland:
	nimble build $(EXTRA_FLAGS) -d:debug -d:wayland $(EXE_NAME)

debug-run: debug-build
	./$(EXE_NAME) $(DEBUG_ARGS)

debug-run-wayland: debug-build-wayland
	./$(EXE_NAME) $(DEBUG_ARGS)

build-release:
	nimble build $(EXTRA_FLAGS) $(RELEASE_FLAGS) -d:release $(EXE_NAME)

build-danger:
	nimble build $(EXTRA_FLAGS) $(RELEASE_FLAGS) -d:danger $(EXE_NAME)

build-release-wayland:
	nimble build $(EXTRA_FLAGS) $(RELEASE_FLAGS) -d:release -d:wayland $(EXE_NAME)

build-danger-wayland:
	nimble build $(EXTRA_FLAGS) $(RELEASE_FLAGS) -d:danger -d:wayland $(EXE_NAME)

perform-install:
	mkdir -p $(INSTALL_FOLDER) && true
	cp $(EXE_NAME) $(INSTALL_FOLDER)
	@echo "Intallation commands executed"

install: clean build-release
	$(MAKE) perform-install

install-wayland: clean build-release-wayland
	$(MAKE) perform-install

install-danger: clean build-danger
	$(MAKE) perform-install

install-danger-wayland: clean build-danger-wayland
	$(MAKE) perform-install

uninstall: clean
	rm $(shell echo "$(INSTALL_FOLDER)$(EXE_NAME)")
	@echo "Uninstalled sucessfully"

clean:
	@if [ -f $(EXE_NAME) ]; then \
		rm $(EXE_NAME); \
	fi

