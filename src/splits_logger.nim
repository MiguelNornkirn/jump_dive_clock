#    jump_dive_clock  -  Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import timer/splits
import parsetoml
import std/[strutils, times, strformat]

proc mbToB(x: int): int = x * 1024 * 1024

const MaxFileSizeBytes = mbToB(10)

var loggingEnabled = false
var logFile: File = nil
var logPath = ""

proc initLogger*(logFilePath: string) =
  loggingEnabled = true
  logPath = logFilePath
  logFile = open(logFilePath, fmAppend)

proc lineSeqToStr(lines: seq[string]): string =
  var first = true

  for line in lines:
    result &= line
    if not first:
      result &= '\n'
    first = false

proc trimLogs(logSizeBytes: int) =
  # Removes the older ~90% of lines of a large log file to save space
  if logSizeBytes > MaxFileSizeBytes:
    var lines = readFile(logPath).split('\n')
    let newLen = int(lines.len() / 10)
    lines = lines[lines.len() - newLen .. ^1]

    let content = lineSeqToStr(lines)
    logFile = open(logPath, fmWrite)
    logFile.write(content)
    logFile.close()
    echo("Trimmed log file")

proc deinitLogger*() =
  if logFile == nil: return
  let fileSizeBytes = logFile.getFileSize()
  if logFile != nil:
    logFile.close()

  trimLogs(fileSizeBytes)
  
proc logSplits*(splits: Splits) =
  let
    date = getDateStr()
    time = getClockStr()
    # The text is tab-indented for readability reasons
    tableText = "\t" & ($(?*splits)).replace("\n", "\n\t")
    toLog = &"\n\n[{date} {time}] {splits.gameName} -" & 
            &" {splits.category}\n{tableText}\n" 

  when defined(debug):
    stdout.write(toLog)

  if loggingEnabled:
    logFile.write(toLog)
