#    jump_dive_clock -  Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import std/[strformat, strutils]
import errors

type Args* = ref object
  configFolder*, splitName*: string

proc parseArg(arg, keyName: string): string =
  if not arg.contains(':'):
    return ""

  let tokens = arg.split(':')
  if tokens[0] != keyName:
    return ""

  if tokens.len() > 2:
    return ""

  result = tokens[1]

## Parses an argument and assigns it to the correct key.
## Will set argIsValid to true if the argument was parsed sucessfully.
template assignKey(argv, keyName: string, key: untyped, argIsValid: bool) =
  let a = parseArg(arg, keyName)
  if a != "":
    key = a
    argIsValid = true

proc parseCliArgs*(args: seq[string]): tuple[v: Args, r: Result] =
  result.v = Args()
  result.r = Result(success: true) # True unless something bad happens

  for arg in args:
    var argIsValid = false

    assignKey arg, "config_folder", result.v.configFolder, argIsValid
    assignKey arg, "split", result.v.splitName, argIsValid

    if not argIsValid:
      result.r.errMsg = fmt"Can not parse {arg}"
      result.r.success = false
      break

  # If nothing failed yet
  if result.r.success:
    if result.v.splitName == "":
      result.r.errMsg = "Split name was not supplied"
      result.r.success = false
