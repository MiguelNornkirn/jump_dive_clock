#    jump_dive_clock -  Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
from raylib import nil
import styling/timer_color

type
  Font* = raylib.Font
  Vector2* = object
    x*, y*: float

var
  considerHiDpi: bool
  drawScale: float

converter timerColortoRaylibColor(x: Color): raylib.Color =
  result = raylib.Color(r: uint8(x.r), g: uint8(x.g), b: uint8(x.b),
                      a: uint8(x.a))

converter raylibVector2ToVector2(v: raylib.Vector2): Vector2 =
  result = Vector2(x: v.x, y: v.y)

converter Vector2ToRaylibVector2(v: Vector2): raylib.Vector2 =
  result = raylib.Vector2(x: v.x, y: v.y)

proc initWindow*(title: string, width, height: int, accountForHiDpi: bool,
                 drawingScale: float) =
  raylib.initWindow(width.int32, height.int32, title)

  considerHiDpi = accountForHiDpi
  drawScale = drawingScale

  if considerHiDpi:
    raylib.setConfigFlags(raylib.flags(raylib.WindowHighDpi))

proc closeWindow*() = raylib.closeWindow()

proc loadFont*(path: string): Font =
  result = raylib.loadFont(path)

proc getTextSize*(text: string, fontSize, fontSpacing: float, font: Font): Vector2 =
  result = raylib.measureText(font, text.cstring, fontSize, fontSpacing)

proc drawText*(text: string, pos: Vector2, fontSize, fontSpacing: float,
               font: Font, color: Color) =
  raylib.drawText(raylib.Font(font), text.cstring, pos, fontSize.float32, fontSpacing.float32, color)

proc drawRectangle*(pos: Vector2, width, height: int, color: Color) =
  raylib.drawRectangle(pos.x.int32, pos.y.int32, width.int32, height.int32,
                       color)

proc makeWindowResizable*() =
  raylib.setConfigFlags(raylib.flags(raylib.WindowResizable))

proc lockFpsAt*(fps: int) =
  raylib.setTargetFps(fps.int32)

proc getRenderingWidth*(): int =
  let w = float(if considerHiDpi: raylib.getRenderWidth()
                else: raylib.getScreenWidth())
  result = int32(w * drawScale)

proc getRenderingHeight*(): int =
  let h = float(if considerHiDpi: raylib.getRenderHeight()
                else: raylib.getScreenHeight())
  result = int32(h * drawScale)

proc clearScreen*(color: Color) =
  raylib.clearBackground(color)

proc isAppWindowFocused*(): bool = raylib.isWindowFocused()

proc getFrameDeltaTime*(): float = raylib.getFrameTime()

proc shouldCloseWindow*(): bool = raylib.windowShouldClose()

template drawCall*(body: untyped): untyped =
  raylib.beginDrawing()
  body
  raylib.endDrawing()

