#    jump_dive_clock -  Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import keybindings
import keycodes
import std/[streams, tables, times, sequtils, sugar, os, osproc, strformat,
            strutils]
import ../utils

const
  # IE means input event
  IETypeKey = 1
  IEValueKeypress = 1
  IEValueAutoRepeat = 2

var
  shouldClose: bool # Flag used for threading
  keyDownTimes: seq[tuple[unixTime: float, key: Keycode]]
  keyReleasedTimes: seq[tuple[unixTime: float, key: Keycode]]

  cfgCh: Channel[Keybindings]
  inputCh: Channel[Table[KeyCode, bool]]

  keyboardThreads: seq[Thread[string]]

  # Contains: [keycode, whether the key is pressed]
  inputTable {.threadvar.}: Table[Keycode, bool]

  timerKeys {.threadvar.}: Keybindings
  strm {.threadvar.}: FileStream

## Returns the /dev/ paths
proc getKeyboardIds(): seq[string] =
  for kind, path in walkDir("/dev/input/"):
    if kind != pcFile: continue
    let (output, err) = execCmdEx(fmt"udevadm info --name={path}")
    if err == 0 and output.contains("ID_INPUT_KEYBOARD=1") and
        output.contains("ID_SERIAL=noserial"):
      echo(fmt"Detected keyboard: {path}")
      result.add(path)

proc getDev(keyboardId: string) {.thread.} =
  echo(fmt"Attempting to get device {keyboardID}")

  let r = cfgCh.recv()
  timerKeys = r
  # Close it if all threads already received its messages
  if cfgCh.peek() == 0:
    cfgCh.close()
    echo("Configuration sharing channel closed")

  let perms = getFilePermissions(keyboardId)
  if perms.contains(fpUserRead):
    strm = newFileStream(keyboardId, fmRead)
    echo("Opened file stream to read from: ", keyboardId)
  else:
    raise newException(OSError,
      fmt"""Failed to open stream to read from {keyboardId}.
        This is probably caused by  not having enough permissions to read from
        the /dev/input/ folder (in this case, add your user to the "input"
        group)""")

proc registerKeycodes() {.thread.} =
  inputTable = initTable[KeyCode, bool]()
  inputTable[timerKeys.redo.keyId] = false
  inputTable[timerKeys.reset.keyId] = false
  inputTable[timerKeys.split.keyId] = false
  inputTable[timerKeys.undo.keyId] = false
  inputTable[timerKeys.lockTimer.keyId] = false
  inputTable[timerKeys.skipSegment.keyId] = false
  inputTable[Rightshift] = false
  inputTable[Rightctrl] = false
  inputTable[Leftshift] = false
  inputTable[Leftctrl] = false

proc isValueDown(inputEventValue: int32): bool =
  result = (inputEventValue == IEValueKeypress or
            inputEventValue == IEValueAutoRepeat)

proc updateInputTable() =
  # We need to make sure there's a message, or else the thread will block and
  # start waiting for user input (which would be bad)
  while inputCh.peek() > 0:
    inputTable = inputCh.recv()

## Update input table before calling
proc isAnyOfDown(id0, id1: Keycode): bool =
  result = inputTable.getOrDefault(id0, false) or
           inputTable.getOrDefault(id1, false)

## Update input table before calling
proc isShiftDown(): bool =
  result = isAnyOfDown(Rightshift, Leftshift)

## Update input table before calling
proc isCtrlDown(): bool =
  result = isAnyOfDown(Rightctrl, Leftctrl)

proc areRequiredExtraKeysPressed(k: Keybinding): bool =
  result = (not k.requiresShift or isShiftDown()) and
           (not k.requiresCtrl or isCtrlDown())

proc genFilter(now, maxTimeDiff: float): auto =
  result = (v: tuple[unixTime: float, key: KeyCode]) =>
             diff(now, v.unixTime) <= maxTimeDiff

proc cleanupKeypressTimes(now: float) =
  keyDownTimes = keyDownTimes.filter(genFilter(now, 1.0))
  keyReleasedTimes = keyReleasedTimes.filter(genFilter(now, 0.5))

proc pressedKeyRecently(key: KeyCode): bool =
  for kptm in keyDownTimes:
    if kptm.key == key:
      return true

proc releasedKeyRecently(key: KeyCode): bool =
  for krtm in keyReleasedTimes:
    if krtm.key == key:
      return true

# The most convenient way I could find of reading global input was /dev/input/.
# Please note that reading from /dev/input/ requires the user to be in
# "input" group (or having root privileges, but that is generally a bad idea)
proc updateGlobalInput() {.thread.} =
  # Layout of the struct we are reading:
  # 16 bytes - time (we don't care about this)
  # type - u16 (the type of event)
  # code - u16 (the keycode, found in /usr/include/linux/input-event-codes.h)
  # value - int32 (0 on release, 1 on press, 2 on autorepeat)
  while not shouldClose:
    if strm.atEnd():
      continue # No keys are being pressed

    # Ignore bytes that we will not use
    for i in countUp(1, 16):
      discard strm.readBool()

    let
      evType = strm.readUint16()
      code = strm.readUint16()
      value = strm.readInt32()

    if evType == IETypeKey:
      let keyCode = cast[KeyCode](code)
      if inputTable.contains(keyCode): # Only store useful keys
        inputTable[keyCode] = isValueDown(value)
        inputCh.send(inputTable)

  inputCh.close()
  echo("Closed input listening channel")

proc initInputThread(kbId: string) {.thread.} =
  getDev(kbId)
  registerKeycodes()
  inputCh.open()
  inputCh.send(inputTable)
  updateGlobalInput()

proc initGlobalInput*(kbds: Keybindings) =
  shouldClose = false
  keyDownTimes = @[]
  keyReleasedTimes = @[]

  cfgCh.open()
  let keyboardPaths = getKeyboardIds()
  if keyboardPaths.len() == 0:
    echo("Could not detect any connected keyboards")
    
  for i, kb in keyboardPaths:
    keyboardThreads.add(Thread[string]())
    cfgCh.send(kbds) # You have to send the keybindings to each thread
    createThread(keyboardThreads[i], initInputThread, kb)

## If the key is currently being held down
proc isKeyDown*(kb: Keybinding): bool =
  updateInputTable()
  result = inputTable.getOrDefault(kb.keyId, false) and
           kb.areRequiredExtraKeysPressed()

## If the key was just pressed (once)
proc isKeyPressed*(kb: Keybinding, useCooldown: bool = true): bool =
  let now = epochTime()

  let keyDownNow = isKeyDown(kb)
  if not keyDownNow:
    keyReleasedTimes.add((now, kb.keyId))
    return false

  cleanupKeypressTimes(now)

  if (not useCooldown or not pressedKeyRecently(kb.keyId)) and
      releasedKeyRecently(kb.keyId):
    keyDownTimes.add((now, kb.keyId))
    result = kb.areRequiredExtraKeysPressed()

proc deinitGlobalInput*(): bool =
  shouldClose = true
