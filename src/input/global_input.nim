#    jump_dive_clock -  Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import keybindings
from linux_input import nil

# Add conditions for more platforms if needed
let
  initGlobalInput*: proc(kbds: Keybindings) = (
    when defined(linux): linux_input.initGlobalInput)
  isKeyDown*: proc(kb: Keybinding): bool = (
    when defined(linux): linux_input.isKeyDown)
  isKeyPressed*: proc(kb: Keybinding, useCooldown: bool = true): bool = (
    when defined(linux): linux_input.isKeyPressed)
  ## Note: we are returning a useless bool in order to work around a compiler
  ## bug. Compilation fails if a void proc is used
  deinitGlobalInput*: proc(): bool = (
    when defined(linux): linux_input.deinitGlobalInput)

