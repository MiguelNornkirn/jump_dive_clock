#    jump_dive_clock -  Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import messages
import stats
import timer_color

type
  TimerStyle* = ref object
    styleVersion*: float
    messages*: Messages
    attemptCountFontSize*: float
    attemptCountFontSpacing*: float
    attemptCountTextPosX*: float
    attemptCountTextPosY*: float
    categoryTitleFontSize*: float
    categoryTitleFontSpacing*: float
    defaultWindowWidth*: int
    defaultWindowHeight*: int
    detailedTimer*: bool
    detailedTimerSize*: float
    detailedTimerMarginX*: float
    detailedTimerMarginY*: float
    drawTimerContainerTopSeparator*: bool
    extraStats*: seq[StatType]
    fontFile*: string
    gameTitleFontSize*: float
    gameTitleFontSpacing*: float
    headerHeight*: float
    colors*: Colors
    maxSegmentSize*: float
    minSegmentsAheadToShow*: int
    segmentFontSize*: float
    segmentFontSpacing*: float
    segmentMargin*: int
    segmentsPerScreen*: int
    separatorSize*: int
    timerFontSize*: float
    timerFontSpacing*: float
    timerSize*: float
    titleCategoryGap*: float
    comparisonFontSize*: float
    comparisonFontSpacing*: float
    comparisonSpacing*: float
    compareToSob*: bool
    comparisonHeaderFontSize*: float
    comparisonHeaderFontSpacing*: float
    comparisonHeaderMarginX*: float
    comparisonHeaderMarginY*: float
    comparisonHeaderSpacingX*: float
    detailedComparisons*: bool
    alwaysShowLastSegment*: bool
