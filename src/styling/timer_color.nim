#    jump_dive_clock -  Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import std/[strformat, strutils]

type
  # We use int instead of uint8 because parsetoml does not handle
  # uint8s properly for some reason
  Color* = object
    r*, g*, b*, a*: int

  # Note: adding colors require changing the saving code to save the color
  Colors* = ref object
    background*: Color
    detailedTimer*: Color
    paceAheadGaining*, paceAheadLosing*: Color
    paceBehindGaining*, paceBehindLosing*: Color
    paceBest*: Color
    separator*: Color
    textBase*: Color
    pb*: Color

proc `$`*(x: Color): string =
  let
    r = toHex(uint8(x.r))
    g = toHex(uint8(x.g))
    b = toHex(uint8(x.b))

  result = fmt"#{r}{g}{b}"
  if x.a != 0xff:
    let a = toHex(uint8(x.a))
    result &= a

  result = result.toLowerAscii()
