#    jump_dive_clock -  Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import messages
import timer_color
import ../timer/[splits, time_consts]
import ../[app_config, rendering, utils]
import std/[strutils, strformat, tables]

type
  StatType* {.pure.} = enum
    BestPossibleTime
    CurrentPace
    RunsThatReachHere
    PersonalBest
    WorldRecord
    SumOfBest
    BestPaceEver

var
  runSplits: Splits
  timerMessages: Messages
  segmentFontSize, segmentFontSpacing: float
  segmentHeight, segmentMargin: int
  separatorSize: int
  timerColors: Colors

proc initExtraStatsModule*(s: Splits, msgs: Messages,
                segFontSize, segFontSpacing: float,
                segHeight, segMargin: int, colors: Colors) =
  runSplits = s
  timerMessages = msgs
  segmentFontSize = segFontSize
  segmentFontSpacing = segFontSpacing
  segmentHeight = segHeight
  segmentMargin = segMargin
  timerColors = colors

proc getWorldRecord*(): string =
  if runSplits.worldRecordTime == NoWrTime:
    result = timerMessages.nilTime
  else:
    result = timerMessages.worldRecordText %
               [timeToStr(runSplits.worldRecordTime),
                runSplits.worldRecordOwner]

proc getSumOfBest*(): string =
  var r = 0.0
  for seg in runSplits.segments:
    if seg.bestSegmentTimeRel == NilTime:
      return timerMessages.nilTime

    r += seg.bestSegmentTimeRel

  result = timeToStr(r)

proc getPersonalBest*(pbTimeSecs: float): string =
  if pbTimeSecs == NoPbTime:
    return timerMessages.nilTime

  result = timeToStr(pbTimeSecs)

proc getRunsThatReachHere*(runStarted: bool, currentSegment: int): string =
  if not runStarted:
    return "****"

  if currentSegment > 0:
    var resetC = 0
    for i in 0 ..< currentSegment:
      resetC += runSplits.segments[i].resetCount
    let p = (runSplits.attemptCount - resetC) /
              max(runSplits.attemptCount, 1) * 100.0
    result = fmt"{p:.2f}%"
  else:
    result = "100%"

proc getBestPossibleTime*(): string =
  # The best possible time is the completed time of the most recent completed
  # segment + the sum of best of the remaining segments
  var tt = 0.0
  var lastFinishedI = -1
  for i in countdown(runSplits.segments.len() - 1, 0):
    if runSplits.segments[i].bestSegmentTimeRel == NilTime:
      return timerMessages.nilTime

    if runSplits.segments[i].isCompleted():
      tt = runSplits.segments[i].completedTimeAbs
      lastFinishedI = i
      break

  # It's unnecessary to add anything if it's the last segment
  if lastFinishedI + 1 < runSplits.segments.len():
    for i in countup(lastFinishedI + 1, runSplits.segments.len() - 1):
      tt += runSplits.segments[i].bestSegmentTimeRel

  result = timeToStr(tt, detailedMode = false)

proc getCurrentPace*(pbTimeSecs: float): string =
  var currentI = ClearTimeIndex
  for i in countup(0, runSplits.segments.len() - 1):
    if not runSplits.segments[i].ranSegmentBefore():
      return timerMessages.nilTime
    if runSplits.segments[i].isCompleted():
      currentI = i

  # If no segment is completed, just use the PB time as an estimate
  if currentI == ClearTimeIndex:
    let lastI = runSplits.segments.len() - 1
    return timeToStr(runSplits.segments[lastI].pbCompletedTimeAbs,
                     detailedMode = false)

  let seg = runSplits.segments[currentI]
  if seg.skippedSegment:
    return timerMessages.nilTime
  
  let t = seg.completedTimeAbs - seg.pbCompletedTimeAbs
  result = timeToStr(pbTimeSecs + t, detailedMode = false)

proc getStatName*(s: StatType): string =
  let nameTable = {
    StatType.BestPossibleTime: timerMessages.bestPossibleTime,
    StatType.CurrentPace: timerMessages.currentPace,
    StatType.RunsThatReachHere: timerMessages.runsThatReachHere,
    StatType.PersonalBest: timerMessages.personalBest,
    StatType.WorldRecord: timerMessages.worldRecord,
    StatType.SumOfBest: timerMessages.sumOfBest,
    StatType.BestPaceEver: timerMessages.bestPaceEver
  }.toTable()
  result = nameTable[s]

proc getBestPaceEver(runStarted: bool, currentSegment: int,
                     pbTimeSecs: float): string =
  if not runStarted:
    return timerMessages.bestPaceNil

  if currentSegment == 0:
    return timerMessages.bestPaceNo

  let runFinished = currentSegment >= runSplits.segments.len()
  
  if runFinished:
    let lastSeg = runSplits.segments[runSplits.segments.len() - 1]
    result = if lastSeg.completedTimeAbs < pbTimeSecs: timerMessages.bestPaceYes
             else: timerMessages.bestPaceNo
  else:
    let currentSeg = runSplits.segments[currentSegment]

    if currentSeg.bestEntryTimeAbs == NilTime:
      return timerMessages.bestPaceNil

    return if currentSeg.startedSegmentTimeAbs <= currentSeg.bestEntryTimeAbs:
               timerMessages.bestPaceYes 
             else: timerMessages.bestPaceNo

proc getStatText*(s: StatType, runStarted: bool, pbTimeSecs: float,
                  currentSegment: int): string =
  case s:
  of StatType.BestPossibleTime:
    result = getBestPossibleTime()
  of StatType.CurrentPace:
    result = getCurrentPace(pbTimeSecs)
  of StatType.RunsThatReachHere:
    result = getRunsThatReachHere(runStarted, currentSegment)
  of StatType.PersonalBest:
    result = getPersonalBest(pbTimeSecs)
  of StatType.WorldRecord:
    result = getWorldRecord()
  of StatType.SumOfBest:
    result = getSumOfBest()
  of StatType.BestPaceEver:
    result = getBestPaceEver(runStarted, currentSegment, pbTimeSecs)

proc drawExtraStats*(stats: seq[StatType], pbTimeSecs: float,
                     currentSegment: int, runStarted: bool, font: Font,
                     config: AppConfig) =
  for i, statType in stats:
    let
      statName = statType.getStatName()
      statNameSize = getTextSize(statName, segmentFontSize, segmentFontSpacing,
                                 font)
      leftTextDrawPos = Vector2(x: segmentMargin.float,
                                y: getRenderingHeight().float - (
                                     (i + 1) * segmentHeight + (i + 1) *
                                     separatorSize).float +
                                     (segmentHeight.float -
                                             statNameSize.y) /
                                     2.0)
      statTxt = statType.getStatText(runStarted, pbTimeSecs, currentSegment)
      statTimeSize = getTextSize(statTxt, segmentFontSize, segmentFontSpacing,
                                 font)
      statTxtPos = Vector2(x: (getRenderingWidth() -
                               segmentMargin).float -
                              statTimeSize.x,
                           y: leftTextDrawPos.y)

    drawText(fmt"{statName}", leftTextDrawPos,
             segmentFontSize, segmentFontSpacing, font, timerColors.textBase)
    drawText(statTxt, statTxtPos, segmentFontSize,
             segmentFontSpacing, font, timerColors.textBase)

