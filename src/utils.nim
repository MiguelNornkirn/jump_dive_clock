#    jump_dive_clock -  Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# TODO: handle negative times
import std/[strformat, strutils, unicode]
import timer/time_consts

proc `mod`*(x: float, y: int): float = x - ((x.int div y) * y).float

func diff*[T](x, y: T): T = (if x > y: x - y else: y - x)

proc timeToStr*(timeSecs: float, detailedMode = false): string =
  if timeSecs == NilTime: return "None"

  const
    MinuteInSecs = 60
    HourInSecs = MinuteInSecs * 60
    Separator = ':'

  var
    ss: float = if timeSecs < MinuteInSecs: timeSecs
                else: timeSecs mod MinuteInSecs
  let
    hh = if timeSecs >= HourInSecs: (timeSecs / HourInSecs).int else: 0
    mm = if timeSecs < HourInSecs: (timeSecs / MinuteInSecs).int
         else: ((timeSecs - (hh * HourInSecs).float) / MinuteInSecs).int

  if hh > 0:
    result &= fmt"{hh}{Separator}"

  result &= fmt"{mm}{Separator}"

  if ss.int < 10:
    result &= "0"

  # Rounding .9X to .90 fixes some weird edge cases that could happen due to
  # the Nim formatter rounding fractions > 0.95 to 1.0
  if ss - ss.int.float > 0.9:
    ss = ss.int.float + 0.9

  if detailedMode:
    result &= fmt"{ss:.1f}"
  else:
    result &= $(ss.int)

## String must be in the hh:mm:ss.ms format.
## Returns NilTime on fail.
proc strToTimeSecs*(timeStr: string): float =
  let unitTokens = timeStr.split(':')

  if unitTokens.len() > 3:
    # Not in the *:*:* format
    echo(fmt"Failed to parse {timeStr} as a time.")
    echo("Not in the [hh:][mm:]ss[.ms] format")
    return NilTime

  var hours, mins, secs: float

  # If it includes hours
  if unitTokens.len() == 3: # hh:mm:ss.ms
    # Parse as int to prevent nonsense like 1.23:45:67.890
    hours = unitTokens[0].parseInt().float
  else:
    hours = 0

  # If it includes minutes
  if unitTokens.len() > 1:
    let i = if hours == 0: 0 else: 1
    # Parse as int to prevent nonsense like 1.23:3.456
    mins = unitTokens[i].parseInt().float
  else:
    mins = 0

  secs = unitTokens[unitTokens.len() - 1].parseFloat()

  result = secs + mins * 60.0 + hours * 60.0 * 60.0

proc convertCamelToSnakeCase*(s: string): string =
  let runes = s.toRunes()
  for r in runes:
    if r.isUpper():
      result &= '_'
    result &= r.toLower()

proc convertSnakeToCamelCase*(s: string): string =
  let chars = cast[seq[char]](s)
  var nextIsCaps = false
  for sc in chars:
    if sc == '_':
      nextIsCaps = true
      continue

    var c = sc
    if nextIsCaps:
      c = sc.toUpperAscii()
      nextIsCaps = false

    result &= c

