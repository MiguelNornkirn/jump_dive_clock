#    jump_dive_clock - Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import std/os

let defaultConfigFolder* = getEnv("HOME") & "/.config/jump-dive-clock/"

type AppConfig* = ref object
  configVersion*: float
  maxBackups*: int
  windowResizable*: bool
  hotReloading*: bool
  considerHiDpi*: bool
  drawScale*: float
  logToFile*: bool

proc getDefaultConfig*(currentVer: float): AppConfig =
  ## Note: version must still be defined elsewhere.
  result = AppConfig(
    configVersion: currentVer,
    maxBackups: 500,
    windowResizable: false,
    hotReloading: false,
    considerHiDpi: true,
    drawScale: 1.0,
    logToFile: false
  )

