#    jump_dive_clock  -  Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import std/math
import timer_drawing
import speedrun_timer
import split_history
import splits
import time_consts
import ../storage/parsing
import ../[app_config, splits_logger, rendering]
import ../storage/storage_manager
import ../styling/[timer_style, messages]
import ../input/[keybindings, global_input]

type TimeSavingResult = enum
  NoPersonalBest
  NewPersonalBest

var
  timer: SpeedrunTimer = SpeedrunTimer()
  splitsFilePath: string
  styleFilePath: string

proc saveTimes(): TimeSavingResult =
  result = NoPersonalBest # Default

  for i, seg in timer.splits.segments:
    let prev = if i > 0: timer.splits.segments[i - 1] else: nil
    seg.updateBestSegment(seg.completedTimeAbs, prev)

  if timer.isRunFinished():
    let pb = timer.finishTime < timer.pbTimeSecs or timer.pbTimeSecs == NoPbTime
    if pb:
      result = NewPersonalBest
      for i, seg in timer.splits.segments:
        seg.pbCompletedTimeAbs = seg.completedTimeAbs
        seg.pbTimeRel = calculatePbRelTime(timer.splits.segments, i)

    let nilWr = timer.splits.worldRecordTime == NoPbTime
    if timer.timeSinceStartSecs < timer.splits.worldRecordTime or nilWr:
      timer.splits.worldRecordTime = timer.timeSinceStartSecs
      timer.splits.worldRecordOwner = timer.style.messages.me

    timer.splits.completedRunBefore = true

  # If a segment was initially skipped, we need to make sure to save it as
  # skipped. Or it will save the time incorrectly
  for seg in timer.splits.segments:
    seg.skippedSegment = seg.initiallySkippedSegment

  timer.splits.saveSplits(splitsFilePath)

proc reset*(initialReset: bool = false) =
  if not timer.hasStarted():
    return

  var runWasPb = false
  if not initialReset:
    if not timer.isRunFinished():
      inc timer.splits.segments[timer.currentSegment].resetCount

    let res = saveTimes()
    runWasPb = res == NewPersonalBest

  timer.pbTimeSecs = timer.timeSinceStartSecs
  timer.timeSinceStartSecs = 0
  timer.currentSegment = ClearTimeIndex
  clearHistory()
  # Note: pbTimeSecs is being passed as a var
  initSegments(timer.splits, runWasPb, timer.pbTimeSecs)

proc initTimer*(config: AppConfig, style: TimerStyle, splits: Splits,
                fontPath: string, stylePath: string, splitsPath: string,
                timerKeymap: Keybindings) =
  timer.config = config
  timer.style = style
  timer.splits = splits
  timer.keymap = timerKeymap
  styleFilePath = stylePath
  splitsFilePath = splitsPath
  
  initDrawing(timer, fontPath)

  reset(initialReset = true)
  initGlobalInput(timer.keymap)

  if config.logToFile:
    initLogger(defaultConfigFolder & "log.log")

proc handleHotReloading() =
  if not timer.config.hotReloading:
    return

  let (s, r) = loadFrom[TimerStyle](styleFilePath, log = false)
  if r.success:
    timer.style = s
  else:
    echo("Failed to load style")
    echo(r.errMsg)

proc handleLocalInput() =
  if not isAppWindowFocused():
    return

  if isKeyPressed(timer.keymap.lockTimer) and
      timer.currentSegment == ClearTimeIndex:
    timer.locked = not timer.locked

proc split() =
  # Splitting does not do anything if the run ended
  if timer.currentSegment >= timer.splits.segments.len():
    return

  if timer.hasStarted():
    timer.splits.segments[timer.currentSegment].finishSegment(
      timer.timeSinceStartSecs
    )

  if timer.currentSegment < 0:
    # Start the run in case it wasn't started
    timer.currentSegment = 0
  else:
    inc timer.currentSegment

  if timer.currentSegment == 0:
    inc timer.splits.attemptCount

  if timer.currentSegment < timer.splits.segments.len():
    timer.splits.segments[timer.currentSegment].beginSegment(
      timer.timeSinceStartSecs
    )

  if timer.isRunFinished():
    timer.finishTime = timer.timeSinceStartSecs

  clearHistory()

  logSplits(timer.splits)

proc skipSegment() =
  let onLastSegmentOrLater = timer.currentSegment >=
          timer.splits.segments.len() - 1
  if not timer.hasStarted() or onLastSegmentOrLater:
    return

  var currentSegment = timer.splits.segments[timer.currentSegment]
  currentSegment.completedTimeAbs = timer.timeSinceStartSecs
  currentSegment.skippedSegment = true
  inc timer.currentSegment

  clearHistory()

proc undo() =
  if timer.currentSegment > 0:
    dec timer.currentSegment
    let seg = timer.splits.segments[timer.currentSegment]
    registerUndo(seg.completedTimeAbs, seg.skippedSegment)
    timer.splits.segments[timer.currentSegment].undoSplit()

proc redo() =
  if not timer.hasStarted() or timer.isRunFinished() or isHistoryEmpty():
    return

  let redoResult = split_history.redo()
  var redoingSeg = timer.splits.segments[timer.currentSegment]
  redoingSeg.finishSegment(redoResult.time)
  redoingSeg.skippedSegment = redoResult.skippedSegment

  inc timer.currentSegment

  # If we are redoing the last segment, the timer needs to be rolled back
  # because it thinks that the run already ended
  if timer.currentSegment == timer.splits.segments.len():
    timer.timeSinceStartSecs = redoResult.time

proc handleGlobalInput() =
  if timer.locked:
    return

  if isKeyPressed(timer.keymap.split):
    split()

  if isKeyPressed(timer.keymap.reset):
    reset()

  if isKeyPressed(timer.keymap.undo):
    undo()

  if isKeyPressed(timer.keymap.redo):
    redo()

  if isKeyPressed(timer.keymap.skipSegment):
    skipSegment()

proc updateTimer*() =
  handleHotReloading()
  handleLocalInput()
  handleGlobalInput()

  if timer.currentSegment != ClearTimeIndex:
    timer.timeSinceStartSecs += getFrameDeltaTime()

proc deinitTimer*() =
  discard deinitGlobalInput()
  deinitLogger()
