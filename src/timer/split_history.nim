#    jump_dive_clock -  Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import time_consts

var undoSplitTimes*: seq[tuple[time: float, skippedSegment: bool]] = @[]

proc pop(): tuple[time: float, skippedSegment: bool] =
  let removeI = undoSplitTimes.len() - 1
  result = undoSplitTimes[removeI]
  undoSplitTimes.delete(removeI)

proc push(time: float, skippedSegment: bool) =
  undoSplitTimes.add((time, skippedSegment))

proc canRedo*(): bool =
  result = undoSplitTimes.len() > 0

proc clearHistory*() =
  undoSplitTimes = @[]

proc isHistoryEmpty*(): bool =
  result = undoSplitTimes.len() == 0

## Returns (NilTime, false) on fail.
proc redo*(): tuple[time: float, skippedSegment: bool] =
  result = if canRedo(): pop() else: (NilTime, false)

proc registerUndo*(time: float, skippedSegment: bool) =
  push(time, skippedSegment)

