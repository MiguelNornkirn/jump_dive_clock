#    jump_dive_clock -  Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
const
  ClearTimeIndex* = -99

  # We use this instead of simply -1 so we can have reliable suport for splits
  # with negative times. This should be high enough because I suppose no one
  # would make a run start after a time longer than the age of the universe
  MagicNumber = -0.0123e18

  NoPbTime* = MagicNumber
  NoWrTime* = MagicNumber
  NilTime* = MagicNumber

