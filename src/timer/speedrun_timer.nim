#    jump_dive_clock  -  Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import ../styling/timer_style
import ../input/keybindings
import std/math
import splits, time_consts
import ../app_config

type SpeedrunTimer* = ref object
  config*: AppConfig
  style*: TimerStyle
  keymap*: Keybindings
  splits*: Splits
  currentSegment*: int

  # timeSinceStartSecs is incremented even after the run ends, only stopping
  # with a reset. Finish time is set once when the run finishes
  timeSinceStartSecs*: float
  finishTime*: float

  pbTimeSecs*: float
  locked*: bool

proc hasStarted*(timer: SpeedrunTimer): bool =
  result = timer.currentSegment > ClearTimeIndex

proc isRunFinished*(timer: SpeedrunTimer): bool =
  result = timer.currentSegment >= timer.splits.segments.len()

