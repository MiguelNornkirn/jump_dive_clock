#    jump_dive_clock  -  Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

# REFACTOR: procs related to stats should be in stats.nim
# Manages the drawing code of the timer
import speedrun_timer
import std/[math, os, strformat]
import ../styling/[timer_color, stats, timer_style]
import ../timer/splits
import ../[utils, rendering]

var
  timer: SpeedrunTimer
  font: Font
  segmentsToDraw: int
  effectiveHeight: int
  headerHeight: int
  timerHeight: int
  segmentHeight: int
  pbComparisonTitleSize: Vector2
  sobComparisonTitleSize: Vector2

proc isDoingMultipleComparisons(style: TimerStyle): bool = style.compareToSob

proc initMathValues() =
  if timer.style.isDoingMultipleComparisons():
    pbComparisonTitleSize = getTextSize(timer.style.messages.comparisonPb,
                                        timer.style.comparisonHeaderFontSize,
                                        timer.style.comparisonHeaderFontSpacing,
                                        font)
    sobComparisonTitleSize = getTextSize(timer.style.messages.comparisonSob,
                                         timer.style.comparisonHeaderFontSize,
                                         timer.style.comparisonHeaderFontSpacing,
                                         font)
    assert pbComparisonTitleSize.y == sobComparisonTitleSize.y
  else:
    pbComparisonTitleSize = Vector2(x: 0, y: 0)
    sobComparisonTitleSize = Vector2(x: 0, y: 0)

  segmentsToDraw = min(timer.splits.segments.len(),
                           timer.style.segmentsPerScreen)
  effectiveHeight = getRenderingHeight() -
                          timer.style.separatorSize * (segmentsToDraw - 1)
  headerHeight = (effectiveHeight.float * timer.style.headerHeight / 100.0 +
                    pbComparisonTitleSize.y).int
  timerHeight = (effectiveHeight.float * timer.style.timerSize / 100.0).int
  effectiveHeight -= headerHeight + timerHeight
  segmentHeight = min((effectiveHeight /
                        (segmentsToDraw + timer.style.extraStats.len(
                          ))).int,
                      int(timer.style.maxSegmentSize / 100 *
                            effectiveHeight.float))

proc handleFont(fontPath: string) =
  # If we don't load a font, Raylib will pick one by default
  if fileExists(fontPath):
    font = loadFont(fontPath)
  else:
    echo(fmt"Font file '{fontPath}' not found. The default font will be loaded")
    font = loadFont("fonts/public_pixel.ttf")

proc initDrawing*(appTimer: SpeedrunTimer, fontPath: string) = 
  timer = appTimer
  handleFont(fontPath)
  initMathValues()

  initExtraStatsModule(timer.splits,
                       timer.style.messages, timer.style.segmentFontSize,
                       timer.style.segmentFontSpacing, segmentHeight,
                       timer.style.segmentMargin, timer.style.colors)

proc horizontallyCollidesWith(text0pos: float, text0width: float,
                              text1pos: float): bool =
  result = text0pos + text0width >= text1pos

proc ranAllSegmentsBefore(): bool =
  result = true
  for seg in timer.splits.segments:
    if not seg.ranSegmentBefore():
      return false

proc getTimerDrawColor(time: float): Color =
  result = Color()
  if time == 0.0 or not ranAllSegmentsBefore():
    return timer.style.colors.textBase

  let segI = min(timer.currentSegment, timer.splits.segments.len() - 1)

  if timer.splits.segments[segI].skippedSegment:
    return timer.style.colors.textBase

  let
    ahead = time < timer.splits.segments[segI].pbCompletedTimeAbs
    lastI = timer.splits.segments.len() - 1

  # In case the run is longer than the PB, there is nothing more to do
  # for this reason, it counts as being behind and losing time
  if not timer.splits.segments[lastI].isAhead(time):
    return timer.style.colors.paceBehindLosing

  if ahead:
    result = if timer.isRunFinished(): timer.style.colors.pb
             else: timer.style.colors.paceAheadGaining
  else:
    # If you are on the last segment and behind, there's nothing you can
    # possibly to do PB, so it makes sense to be use behindLosing
    result = if timer.currentSegment >= timer.splits.segments.len() - 1:
               timer.style.colors.paceBehindLosing
             else:
               timer.style.colors.paceBehindGaining

proc getDetailedTime(): float =
  let time = if isRunFinished(timer): timer.finishTime else: timer.timeSinceStartSecs

  var previousSegmentTime = 0.0
  if timer.currentSegment > 0:
    let
      prevI = timer.currentSegment - (if isRunFinished(timer): 2 else: 1)
      prevSeg = timer.splits.segments[prevI]
    previousSegmentTime = prevSeg.completedTimeAbs
  
  result = time - previousSegmentTime

proc drawDetailedTimer(bottomHeight: int) =
  let
    detailedTime = getDetailedTime()
    detailedTimerFontSize = round(timer.style.timerFontSize *
                                  timer.style.detailedTimerSize).float
    detailedTimerFontSpacing = max(1,
                                   round(timer.style.timerFontSpacing *
                                         timer.style.detailedTimerSize)).float
    detailedTimerText = timeToStr(if timer.locked: 0.0 else: detailedTime,
                                  detailedMode = true)
    detailedTimerTextSize = getTextSize(detailedTimerText,
                                        detailedTimerFontSize,
                                        detailedTimerFontSpacing, font)
    detailedTimerPos = Vector2(x: getRenderingWidth().float -
                                  detailedTimerTextSize.x -
                                  getRenderingWidth().float *
                                  (1 - timer.style.detailedTimerMarginX),
                               y: (getRenderingHeight() -
                                       bottomHeight).float -
                                  timerHeight.float /
                                  (1.0 /
                                    (1 - timer.style.detailedTimerMarginY)) -
                                  detailedTimerTextSize.y / 2.0)
  drawText(detailedTimerText, detailedTimerPos,
           detailedTimerFontSize, detailedTimerFontSpacing,
           font, timer.style.colors.detailedTimer)

proc drawTimer() =
  let
    t = if timer.isRunFinished(): timer.finishTime else: timer.timeSinceStartSecs
    timerText = if timer.locked: timer.style.messages.lockedMode
                else: timeToStr(t, detailedMode = true)
    textSize = getTextSize(timerText, timer.style.timerFontSize,
                           timer.style.timerFontSpacing, font)
    bottomHeight = segmentHeight * timer.style.extraStats.len() +
                    timer.style.separatorSize * (timer.style.extraStats.len() - 1)
    timerTextPos = Vector2(x: (getRenderingWidth().float - textSize.x) * 0.5,
                           y: (getRenderingHeight() - bottomHeight).float -
                                (timerHeight.float + textSize.y) * 0.5)
    timerColor = getTimerDrawColor(t)

  drawText(timerText, timerTextPos, timer.style.timerFontSize,
           timer.style.timerFontSpacing, font, timerColor)

  if timer.style.detailedTimer:
    drawDetailedTimer(bottomHeight)

proc drawSeparators() =
  drawRectangle(Vector2(x: 0, y: headerHeight.float), getRenderingWidth(),
                timer.style.separatorSize, timer.style.colors.separator)

  for i in countup(1, segmentsToDraw):
    drawRectangle(Vector2(x: 0, y: (headerHeight + i *
                    segmentHeight + timer.style.separatorSize * i).float),
                  getRenderingWidth(),
                  timer.style.separatorSize, timer.style.colors.separator)
  let
    timerOffset = timer.style.extraStats.len().int * segmentHeight +
                    (timer.style.extraStats.len() - 1).int *
                    timer.style.separatorSize
    timerY = getRenderingHeight() - timerHeight -
              timer.style.separatorSize - timerOffset

  if timer.style.drawTimerContainerTopSeparator:
    drawRectangle(Vector2(x: 0, y: timerY.float), getRenderingWidth(),
                  timer.style.separatorSize, timer.style.colors.separator)

  drawRectangle(Vector2(x: 0, y: (timerY + timerHeight).float), getRenderingWidth(),
                timer.style.separatorSize,
                timer.style.colors.separator)

  for i in 1 ..< timer.style.extraStats.len():
    drawRectangle(Vector2(x: 0,
                          y: (timerY + timerHeight + i * segmentHeight +
                    timer.style.separatorSize * (i - 1)).float),
                  getRenderingWidth(),
                  timer.style.separatorSize, timer.style.colors.separator)


proc drawSobEnabledHeaderElems() =
  let
    pbText = timer.style.messages.comparisonPb
    sobText = timer.style.messages.comparisonSob
    pbTextSize = getTextSize(pbText,
                             timer.style.comparisonHeaderFontSize,
                             timer.style.comparisonHeaderFontSpacing, font)
    sobTextSize = getTextSize(sobText,
                              timer.style.comparisonHeaderFontSize,
                              timer.style.comparisonHeaderFontSpacing, font)
    pbTextPos = Vector2(x: getRenderingWidth().float - pbTextSize.x -
                          timer.style.comparisonHeaderMarginX,
                        y: headerHeight.float -
                             timer.style.comparisonHeaderMarginY -
                             pbTextSize.y)
    sobPos = Vector2(x: pbTextPos.x - timer.style.comparisonHeaderSpacingX - sobTextSize.x,
                     y: pbTextPos.y)
  drawText(pbText, pbTextPos,
           timer.style.comparisonHeaderFontSize,
           timer.style.comparisonHeaderFontSpacing,
           font, timer.style.colors.textBase)
  drawText(sobText, sobPos,
           timer.style.comparisonHeaderFontSize,
           timer.style.comparisonHeaderFontSpacing,
           font, timer.style.colors.textBase)

## Without SoB
proc drawNormalHeader(gameTitlePos, categoryTitlePos, attemptCountPos: Vector2) =
  drawText(timer.splits.gameName, gameTitlePos,
           timer.style.gameTitleFontSize, timer.style.gameTitleFontSpacing,
           font, timer.style.colors.textBase)
  drawText(timer.splits.category, categoryTitlePos,
           timer.style.categoryTitleFontSize,
           timer.style.categoryTitleFontSpacing, font, timer.style.colors.textBase)
  drawText($timer.splits.attemptCount, attemptCountPos,
           timer.style.attemptCountFontSize,
           timer.style.attemptCountFontSpacing, font, timer.style.colors.textBase)

proc drawHeader() =
  let
    gameTitleSize = getTextSize(timer.splits.gameName,
                                timer.style.gameTitleFontSize,
                                timer.style.gameTitleFontSpacing, font)
    categoryFontSize = getTextSize(timer.splits.category,
                                   timer.style.categoryTitleFontSize,
                                   timer.style.categoryTitleFontSpacing, font)
    attemptCountSize = getTextSize($timer.splits.attemptCount,
                                   timer.style.attemptCountFontSize,
                                   timer.style.attemptCountFontSpacing, font)
    comparisonHeaderHeight = pbComparisonTitleSize.y +
        timer.style.comparisonHeaderMarginY * 2
    textLayoutHeight = gameTitleSize.y + categoryFontSize.y +
                       comparisonHeaderHeight
    textLayoutStartY = (headerHeight.float - textLayoutHeight) / 2.0
    gameTitlePos = Vector2(x: (getRenderingWidth().float -
                               gameTitleSize.x) / 2.0,
                           y: textLayoutStartY)
    categoryTitlePos = Vector2(x: (getRenderingWidth().float -
                                   categoryFontSize.x) / 2.0,
                               y: textLayoutStartY +
                                  timer.style.titleCategoryGap +
                                  gameTitleSize.y)
    attemptCountPos = Vector2(x: (getRenderingWidth().float *
                                  timer.style.attemptCountTextPosX) -
                                  attemptCountSize.x,
                              y: (headerHeight.float *
                                  timer.style.attemptCountTextPosY) -
                                  attemptCountSize.y)

  drawNormalHeader(gameTitlePos, categoryTitlePos, attemptCountPos)

  if timer.style.compareToSob:
    drawSobEnabledHeaderElems()

## Returns the position of the comparison text
proc drawSobComparison(splits: Splits, seg, prev: Segment, completedTimeCmpPos: Vector2): Vector2 =
  if seg.initiallySkippedSegment or seg.skippedSegment or
      not splits.completedRunBefore:
    return
  var
    sobCmpTxt: string
    sobColor: Color
  sobCmpTxt = seg.getTimeCmpText(seg.completedTimeAbs,
                                 seg.sobCompletedTimeAbs,
                                 timer.style.detailedComparisons)
  sobColor = seg.pickColor(prev, timer.splits, timer.style.colors,
                           comparingAgainstSob = true)
  let sobCmpSize = getTextSize(sobCmpTxt,
                             timer.style.comparisonFontSize,
                             timer.style.comparisonFontSpacing, font)

  result = Vector2(x: completedTimeCmpPos.x -
                           timer.style.comparisonFontSpacing -
                           sobCmpSize.x,
                        y: completedTimeCmpPos.y)

  drawText(sobCmpTxt, result,
           timer.style.comparisonFontSize,
           timer.style.comparisonFontSpacing, font, sobColor)

## This makes sure "Segment Name" gets converted to the "Segment..." format
## if the full form overlaps the comparisons
proc resizeName(seg: Segment,
                segmentStartY: int, closestLabelX: float): tuple[name: string,
                                               namePos: Vector2] =
  # Adds "..." at the end of segment name if it's overlapping the comparisons
  var threeDots = false
  result.name = seg.name

  var nameFitsOnScreen: bool
  while not nameFitsOnScreen:
    let nameSize = getTextSize(result.name,
                                    timer.style.segmentFontSize,
                                    timer.style.segmentFontSpacing, font)
    result.namePos = Vector2(x: timer.style.segmentMargin.float,
                             y: float(segmentStartY.float +
                                  (segmentHeight.float -
                                          nameSize.y) / 2.0))

    nameFitsOnScreen = closestLabelX == 0 or
                         not horizontallyCollidesWith(result.namePos.x,
                                                  nameSize.x,
                                                  closestLabelX)
    if not nameFitsOnScreen:
      # Will keep cropping the name with "..." until it fits the screen
      result.name = result.name[0 .. (result.name.len() - 2 - (if threeDots: 3 else: 0))] &
              (if threeDots: "..." else: "")
      if not threeDots:
        result.name &= "..."
        threeDots = true

proc getPbTimeText(seg: Segment, segIndex: int): string =
    if seg.skippedSegment:
      if segIndex >= timer.currentSegment:
        result = timer.style.messages.loadedSkippedSegmentText
      else:
        result = timer.style.messages.skippedSegmentText
    else:
      result = seg.getPbText(timer.splits)

proc getPbCmpColor(seg, prev: Segment): Color =
  if seg.initiallySkippedSegment or seg.skippedSegment:
    result = timer.style.colors.textBase
  else:
    result = seg.pickColor(prev, timer.splits, timer.style.colors)

proc getPbTimeCmp(seg: Segment): string =
  if timer.splits.completedRunBefore and not (seg.initiallySkippedSegment or
                                              seg.skippedSegment):
    result = seg.getTimeCmpText(seg.completedTimeAbs,
                                seg.pbCompletedTimeAbs,
                                timer.style.detailedComparisons)
  else:
    result = timer.style.messages.emptyComparisonText

proc drawSegments(splits: Splits) =
  let offset = min(max(timer.currentSegment -
                        (segmentsToDraw - timer.style.minSegmentsAheadToShow),
                            0),
                   timer.splits.segments.len() - segmentsToDraw)

  for i in countup(offset, timer.splits.segments.len()):
    if i >= offset + segmentsToDraw:
      break

    let
      drawI = i - offset

      isLastSegment = i + 1 >= offset + segmentsToDraw
      seg = if isLastSegment and timer.style.alwaysShowLastSegment:
              timer.splits.segments[timer.splits.segments.len() - 1]
            else: timer.splits.segments[i]

      prev = if i > 0: timer.splits.segments[i - 1] else: nil

      segmentStartY = headerHeight + segmentHeight * drawI +
                      timer.style.separatorSize * (drawI + 1)

      pbTimeText = seg.getPbTimeText(i)
      pbTimeSize = getTextSize(pbTimeText,
                               timer.style.comparisonFontSize,
                               timer.style.comparisonFontSpacing, font)
      pbTimePos = Vector2(x: getRenderingWidth().float - pbTimeSize.x -
                              timer.style.segmentMargin.float,
                          y: float(segmentStartY.float +
                            ((segmentHeight.float - pbTimeSize.y) / 2.0)))

    var
      completedTimeCmpSize: Vector2
      completedTimeCmpPos: Vector2
      sobCmpPos: Vector2

    # Draw comparisons (e.g. "+1:23")
    if seg.completedTimeAbs != 0:
      let pbTimeCmp = seg.getPbTimeCmp()
      completedTimeCmpSize = getTextSize(pbTimeCmp,
                                      timer.style.comparisonFontSize,
                                      timer.style.comparisonFontSpacing, font)
      completedTimeCmpPos = Vector2(x: getRenderingWidth().float -
                                    pbTimeSize.x - completedTimeCmpSize.x -
                                    timer.style.comparisonSpacing,
                                    y: pbTimePos.y)

      if timer.style.compareToSob:
        sobCmpPos = drawSobComparison(splits, seg, prev, completedTimeCmpPos)

      let pbCmpColor = seg.getPbCmpColor(prev)

      drawText(pbTimeCmp, completedTimeCmpPos,
               timer.style.comparisonFontSize,
               timer.style.comparisonFontSpacing, font, pbCmpColor)

    let
      closestLabelX = if timer.style.compareToSob: sobCmpPos.x
                      else: completedTimeCmpPos.x
      (name, segmentNamePos) = resizeName(seg, segmentStartY,
                                          closestLabelX)

    drawText(name, segmentNamePos, timer.style.segmentFontSize,
             timer.style.segmentFontSpacing, font,
             timer.style.colors.textBase)
    drawText(pbTimeText, pbTimePos, timer.style.comparisonFontSize,
             timer.style.comparisonFontSpacing, font,
             timer.style.colors.textBase)

proc drawApp*(splits: Splits) =
  clearScreen(timer.style.colors.background)
  drawSeparators()
  drawHeader()
  drawSegments(splits)
  drawExtraStats(timer.style.extraStats, timer.pbTimeSecs,
                 timer.currentSegment, timer.hasStarted(), font, timer.config)
  drawTimer()

  if timer.config.hotReloading:
    drawText("[!] Hot reloading ON.\n[!]May worsen performance",
             Vector2(x: 5, y: 5),
             6, 1, font, Color(r: 0xff - timer.style.colors.background.r,
                               g: 0xcf - timer.style.colors.background.g,
                               b: 0x9f - timer.style.colors.background.b,
                               a: 0xff))

