#    jump_dive_clock -  Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import ../utils
import ../styling/timer_color
import time_consts

const NoEntryTime* = -2.0

## Properties with this pragma won't be saved. They will be runtime-only
## Note: this is just for readability, you have to change the saving code
## whenever you add or remove this pragma to something
template noSave* {.pragma.}

type
  Segment* = ref object
    name*: string
    resetCount*: int
    pbSplitFinishTime*: string
    bestSegmentRelativeTime*: string
    bestEntryTime*: string
    bestEntryTimeAbs* {.noSave.}: float
    bestSegmentTimeRel* {.noSave.}: float
    pbCompletedTimeAbs* {.noSave.}: float
    completedTimeAbs* {.noSave.}: float
    sobCompletedTimeAbs* {.noSave.}: float
    startedSegmentTimeAbs* {.noSave.}: float
    skippedSegment* {.noSave.}: bool
    initiallySkippedSegment* {.noSave.}: bool
    # May be loaded in legacy configs, but never saved in new files
    # It is also used during runtime in new versions
    pbTimeRel* {.noSave.}: float
    prevBestEntry {.noSave.}: float # Used exclusively for undoing segments

  Splits* = ref object
    gameName*: string
    styleName*: string
    keymapName*: string
    attemptCount*: int
    category*: string
    completedRunBefore*: bool
    maximumFramerate*: int
    worldRecordOwner*: string
    worldRecordTime*: float
    splitsVersion*: float
    segments*: seq[Segment]

proc ranSegmentBefore*(s: Segment): bool =
  result = s.bestSegmentTimeRel != NilTime

proc initSegments*(splits: Splits, wasSessionLastAttemptPb: bool,
                   pbTimeSecs: var float) =
  if wasSessionLastAttemptPb:
    # The old skipped segments of the finished run will determine which
    # segments are initially skipped in this one
    for seg in splits.segments:
      seg.initiallySkippedSegment = seg.skippedSegment

  var absSobTime = 0.0

  # Iterating through all segments to calculate the absolute PB and SoB times
  for i in 0 ..< splits.segments.len():
    absSobTime = 0.0
    for j in countDown(i, 0):
      var seg = splits.segments[j]

      if not seg.ranSegmentBefore():
        absSobTime = NoPbTime
        break

      absSobTime += seg.bestSegmentTimeRel

    var seg = splits.segments[i]
    seg.sobCompletedTimeAbs = absSobTime
    seg.completedTimeAbs = 0.0
    seg.startedSegmentTimeAbs = 0.0
    seg.skippedSegment = seg.initiallySkippedSegment
    seg.prevBestEntry = NoEntryTime

  let lastSeg = splits.segments[splits.segments.len() - 1]
  pbTimeSecs = lastSeg.pbCompletedTimeAbs

proc isCompleted*(seg: Segment): bool =
  result = seg.completedTimeAbs > 0

proc getPbText*(seg: Segment, splits: Splits): string =
  if seg.isCompleted():
    return timeToStr(seg.completedTimeAbs)

  if splits.completedRunBefore:
    result = timeToStr(seg.pbCompletedTimeAbs)
  else:
    result = "**:**"

proc isAhead*(seg: Segment, timeAbs: float): bool =
  result = timeAbs < seg.pbCompletedTimeAbs

proc getTimeCmpText*(seg: Segment, timeAbs, cmpTimeAbs: float,
                     detailed: bool): string =
  result = if timeAbs < cmpTimeAbs: "-" else: "+"
  result &= timeToStr(diff(timeAbs, cmpTimeAbs), detailed)

proc getRelTime(seg: Segment): float =
  result = seg.completedTimeAbs - seg.startedSegmentTimeAbs

proc isBest*(seg: Segment, prev: Segment): bool =
  if seg.skippedSegment or (prev != nil and prev.skippedSegment):
    return false

  result = seg.getRelTime() < seg.bestSegmentTimeRel or
            seg.bestSegmentTimeRel == NilTime

proc updateBestSegment*(seg: Segment, completedTime: float, prev: Segment) =
  if completedTime != 0 and seg.isBest(prev):
    seg.bestSegmentTimeRel = seg.getRelTime()

proc isGainingTimeRel*(seg: Segment): bool =
  result = seg.getRelTime() < seg.pbTimeRel

proc pickColor*(seg, prev: Segment, splits: Splits,
                colors: Colors, comparingAgainstSob: bool = false): Color =
  if not splits.completedRunBefore:
    return colors.paceAheadGaining

  if prev != nil and prev.skippedSegment:
    return colors.textBase

  if seg.isBest(prev):
    if comparingAgainstSob:
      return (if seg.completedTimeAbs < seg.sobCompletedTimeAbs:
               colors.paceAheadGaining
              else: colors.paceBehindGaining)
    else:
      return colors.paceBest
  elif comparingAgainstSob:
    return (if seg.completedTimeAbs < seg.sobCompletedTimeAbs:
             colors.paceAheadLosing
            else:
             colors.paceBehindLosing)

  if seg.completedTimeAbs < seg.pbCompletedTimeAbs:
    result = (if seg.isGainingTimeRel():
                colors.paceAheadGaining
              else:
                colors.paceAheadLosing)
  else:
    result = (if seg.isGainingTimeRel():
                colors.paceBehindGaining
              else:
                colors.paceBehindLosing)

proc beginSegment*(seg: Segment, time: float) =
  seg.startedSegmentTimeAbs = time

  if time < seg.bestEntryTimeAbs or seg.bestEntryTimeAbs == NilTime:
    seg.prevBestEntry = seg.bestEntryTimeAbs 
    seg.bestEntryTimeAbs = time


proc finishSegment*(seg: Segment, time: float) =
  seg.completedTimeAbs = time
  seg.skippedSegment = false

proc undoSplit*(seg: Segment) =
  seg.completedTimeAbs = 0
  seg.skippedSegment = seg.initiallySkippedSegment

  if seg.prevBestEntry != NoEntryTime:
    seg.bestEntryTimeAbs = seg.prevBestEntry

proc calculatePbRelTime*(segs: seq[Segment], currentSegI: int): float =
  if currentSegI >= segs.len(): return
  let current = segs[currentSegI]
  result = current.pbCompletedTimeAbs
  if currentSegI > 0:
    result -= segs[currentSegI - 1].pbCompletedTimeAbs

