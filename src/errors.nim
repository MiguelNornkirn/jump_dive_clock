#    jump_dive_clock -  Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
type
  Result* = ref object
    success*: bool
    errMsg*: string

## Returns the value of the result, unless an error happened, then crash.
proc getOrCrashOnFail*[T](pair: tuple[v: T, r: Result], showUsage = false): T =
  if not pair.r.success:
    echo(pair.r.errMsg)
    if showUsage:
      echo("Usage: jump_dive_clock [config_folder:<folder>] split:<split name>")
    quit(1)
  result = pair.v
