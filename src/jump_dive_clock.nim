#    jump_dive_clock -  Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import std/[os, strformat]
import errors
import rendering
import storage/[storage_manager, parsing, backups]
import app_config
import arg_parser
import styling/timer_style
import input/keybindings
import timer/[timer_controller, timer_drawing, splits]
import tinydialogs

proc setupWindow(defaultWidth, defaultHeight, maxFramerate: int,
                 config: AppConfig) =
  if config.windowResizable:
    makeWindowResizable()

  initWindow("Jump Dive Clock", defaultWidth, defaultHeight,
             config.considerHiDpi, config.drawScale)

  lockFpsAt(maxFramerate)

proc init(args: Args): tuple[splits: Splits, result: Result] =
  result.result = Result(success: true)

  if args.configFolder == "":
    args.configFolder = defaultConfigFolder
    echo("Config folder was not supplied, using ", args.configFolder, " as default")

  let
    splitsDir = fmt"{args.configFolder}/splits"
    stylesDir = fmt"{args.configFolder}/styles"
    keymapsDir = fmt"{args.configFolder}/keymaps"
    backupsDir = fmt"{args.configFolder}/backups"
    fontsDir = fmt"{args.configFolder}/fonts"

  createDir(args.configFolder)
  createDir(splitsDir)
  createDir(stylesDir)
  createDir(backupsDir)
  createDir(fontsDir)
  createDir(keymapsDir)

  let
    configFile = fmt"{args.configFolder}/config.toml"
    splitFile = fmt"{splitsDir}/{args.splitName}.toml"

  if not fileExists(configFile):
    createDefaultConfig(configFile)

  if not fileExists(splitFile):
    result.result.errMsg = fmt"Could not find the {splitFile} file"
    result.result.success = false
    return result

  let
    splits = getOrCrashOnFail(loadFrom[Splits](splitFile))
    config = getOrCrashOnFail(loadFrom[AppConfig](configFile))
    styleFile = fmt"{stylesDir}/{splits.styleName}"
    keymapFile = fmt"{keymapsDir}/{splits.keymapName}"

  if not fileExists(styleFile):
    result.result.errMsg = fmt"Could not find the {styleFile} file"
    result.result.success = false
    return result

  if not fileExists(keymapFile):
    result.result.errMsg = fmt"Could not find the {keymapFile} file"
    result.result.success = false
    return result

  let
    style = getOrCrashOnFail(loadFrom[TimerStyle](styleFile))
    keymap = getOrCrashOnFail(loadFrom[Keybindings](keymapFile))
    fontPath = fmt"{fontsDir}/{style.fontFile}"

  saveStyle(style, styleFile)
  saveAppConfig(config, configFile)
  saveKeymap(keymap, keymapFile)

  setupWindow(style.defaultWindowWidth, style.defaultWindowHeight,
              splits.maximumFramerate, config)

  initTimer(config, style, splits, fontPath, styleFile, splitFile, keymap)
  setupBackups(fmt"{args.configFolder}/backups/{args.splitName}",
               config.maxBackups, args.splitName)

  result.splits = splits

proc mainLoop(splits: Splits) =
  var running = true
  while running:
    updateTimer()
    drawCall:
      drawApp(splits)

    if shouldCloseWindow():
      running = false
      echo("Closing app")
      # Doing this in order to get it to properly finish the run current run
      # and then autosave.
      reset()

let
  rawArgs = commandLineParams()
  parsingResult = parseCliArgs(rawArgs)
  parsedArgs = getOrCrashOnFail(parsingResult, showUsage = true)

let (timerSplits, result) = init(parsedArgs)
if result.success:
  mainLoop(timerSplits)
  deinitTimer()
  closeWindow()
else:
  echo result.errMsg
  discard messageBox("jump_dive_clock", fmt"Failed to initialize program: {result.errMsg}", Ok, Error, Yes)

