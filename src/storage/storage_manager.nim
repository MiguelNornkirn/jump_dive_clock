#    jump_dive_clock - Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import std/[strformat, strutils, os, algorithm]
import ../[utils, app_config]
import ../timer/[splits, time_consts]
import ../styling/[timer_style, timer_color]
import ../input/[keybindings]
import parsing
import backups
import parsetoml

# Assumes that the text is formatted in the way parsetoml formats it
proc tomlStrToSnakeCase(str: string): string =
  let lines = str.split('\n')
  for line in lines:
    let l = strutils.strip(line)

    if l == "":
      result &= '\n'
      continue

    if l[0] == '#': # Comment
      continue

    let tokensBySpace = l.split(' ')
    if l.startsWith('[') and l.endsWith(']'):
      # Lines such as [[foo]] or [bar]
      let
        t = l.split('[')
        n = convertCamelToSnakeCase(t[t.len() - 1].split(']')[0])
        bracketCount = l.count('[')
      for _ in 0 ..< bracketCount: result &= '['
      result &= n
      for _ in 0 ..< bracketCount: result &= ']'
    else:
      # Assignments (i.e. "foo = 123")
      let keyName = convertCamelToSnakeCase(tokensBySpace[0])
      result &= fmt"{keyName} "
      for i in 1 ..< tokensBySpace.len():
        let token = tokensBySpace[i]
        result &= token
        # Add space unless it's the last token
        if i < tokensBySpace.len() - 1:
          result &= ' '

    result &= '\n'

proc createDefaultConfig*(atPath: string) =
  let defaultConfig = getDefaultConfig(CurrentConfigVersion)
  writeFile(atPath, tomlStrToSnakeCase($(?*defaultConfig)))

proc saveDataIfChanged(tomlData, filePath: string, newVer: float) = 
  let
    newData = tomlStrToSnakeCase(tomlData)
    oldData = readFile(filePath)

  if oldData != newData:
    writeFile(filePath, newData)
    echo(fmt"Upgraded {filePath} to the version {newVer}.")

proc toTimeStrOrEmptyTime(time: float): string =
  result = if time == NilTime: EmptyTime
           else: timeToStr(time, detailedMode = true)

proc removeRuntimeVariables(tomlTable: TomlValueRef) =
  # It would be nice to make code that automatically doesn't save values with
  # the {.noSave.} pragma, but I don't understand macros well enough to do that
  # myself :p
  for i in 0 ..< tomlTable["segments"].len():
    let t = tomlTable["segments"][i]
    t.delete("completedTimeAbs")
    t.delete("pbCompletedTimeAbs")
    t.delete("sobCompletedTimeAbs")
    t.delete("startedSegmentTimeAbs")
    t.delete("pbTimeRel")
    t.delete("bestSegmentTimeRel")
    t.delete("skippedSegment")
    t.delete("initiallySkippedSegment")
    t.delete("bestEntryTimeAbs")
    t.delete("prevBestEntry")

proc saveFormattedTimes(segments: seq[Segment]) =
  for seg in segments:
    seg.bestSegmentRelativeTime = toTimeStrOrEmptyTime(seg.bestSegmentTimeRel)
    seg.bestEntryTime = toTimeStrOrEmptyTime(seg.bestEntryTimeAbs)

    if seg.skippedSegment:
      seg.pbSplitFinishTime = SkippedSegment
    else:
      seg.pbSplitFinishTime = toTimeStrOrEmptyTime(seg.pbCompletedTimeAbs)

proc saveSplits*(splits: Splits, path: string) =
  # Upgrade version
  splits.splitsVersion = CurrentSplitsVersion

  saveFormattedTimes(splits.segments)

  var tomlTable = ?*splits

  # This one specifically should be saved manually, for historical reasons
  # It's necessary to convert because the table automatically uses the source
  # code's naming convention
  let k = convertSnakeToCamelCase(WrTimeKey)
  tomlTable[k] = newTString(timeToStr(splits.worldRecordTime,
                                      detailedMode = true))

  if tomlTable.hasKey(OldWrKey):
    tomlTable.delete(OldWrKey)

  tomlTable.removeRuntimeVariables()

  let tomlData = tomlStrToSnakeCase($tomlTable)

  if fileExists(path):
    backupOldSplits(tomlData)

  writeFile(path, tomlData)

proc saveKeymap*(keymap: Keybindings, path: string) =
  keymap.keymapVersion = CurrentKeymapVersion # Upgrade version
  var keymapTbl = ?*keymap

  # Remove deprecated field
  keymapTbl.delete("keyboardId")

  let newSaveData = $keymapTbl
  saveDataIfChanged(newSaveData, path, CurrentKeymapVersion)

proc saveAppConfig*(config: AppConfig, path: string) =
  config.configVersion = CurrentConfigVersion # Upgrade version
  let newData = $(?*config)
  saveDataIfChanged(newData, path, CurrentConfigVersion)

proc saveStyle*(style: TimerStyle, atPath: string) =
  style.styleVersion = CurrentStyleVersion # Upgrade version

  # The extra stats are saved in reverse order. We need to invert them for
  # saving and restore them to their natural state after we're done
  style.extraStats.reverse()
  defer: style.extraStats.reverse()

  var styleTable = ?*style

  # Remove deprecated field
  if styleTable.hasKey("new_segment_text"):
    styleTable.delete("new_segment_text")

  # Saving colors manually so that a more human-friendly format can be used
  styleTable.delete("colors")
  styleTable["colors"] = newTTable()
  var colors = styleTable["colors"]
  colors["background"] = newTString($style.colors.background)
  colors["detailed_timer"] = newTString($style.colors.detailedTimer)
  colors["pace_ahead_gaining"] = newTString($style.colors.paceAheadGaining)
  colors["pace_ahead_losing"] = newTString($style.colors.paceAheadLosing)
  colors["pace_behind_gaining"] = newTString($style.colors.paceBehindGaining)
  colors["pace_behind_losing"] = newTString($style.colors.paceBehindLosing)
  colors["pace_best"] = newTString($style.colors.paceBest)
  colors["separator"] = newTString($style.colors.separator)
  colors["text_base"] = newTString($style.colors.textBase)
  colors["pb"] = newTString($style.colors.pb)
  
  let styleTxt = $styleTable

  saveDataIfChanged(styleTxt, atPath, CurrentStyleVersion)

