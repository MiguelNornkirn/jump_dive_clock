#    jump_dive_clock - Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import std/[macros, strformat, strutils, os, algorithm]
import ../[utils, errors, app_config]
import ../timer/[splits, time_consts]
import ../styling/[timer_style, timer_color, messages, stats]
import ../input/[keybindings, keycodes]
import parsetoml
import tinydialogs

const
  SkippedSegment* = "Skipped"
  EmptyTime* = "None"
  WrTimeKey* = "world_record_time"
  OldWrKey* = "world_record_seconds"

# Versioning
const
  CurrentConfigVersion* = 4.0
  CurrentKeymapVersion* = 7.0
  CurrentSplitsVersion* = 6.0
  CurrentStyleVersion* = 8.0

proc crash(msg: string, title: string = "") =
  if msg != "":
    let title = "Error" & (if title != "": fmt" - {title}" else: "")
    discard messageBox(title, msg, Ok, Error, Yes)
  quit(1)

proc colorStringToColor(str: string): Color =
  ## May be either in the #rrggbbaa or #rrggbb formats
  # The offset accounts for the different formats, we add +2 for #rrggbbaa
  let offset = if str.len() == 9: 2 else: 0

  if offset == 0 and str.len() != 7:
    # If it's not in neither the #rrggbbaa or #rrggbb formats, something's wrong
    crash(fmt"Invalid hex color length {str}")

  try:
    result = Color(
      r: parseHexInt(str[1 .. ^(5 + offset)]),
      g: parseHexInt(str[3 .. ^(3 + offset)]),
      b: parseHexInt(str[5 .. ^(1 + offset)]),
    )
  except ValueError:
    crash(fmt"Invalid hex color {str}")

  if offset > 0: #rrggbbaa
    result.a = parseHexInt(str[7 .. ^1])
  else: #rrggbb
    result.a = 0xff

proc crashIfOutOfRange[T](n, min, max: T, windowTitle: string) =
  if n < min or n > max:
    crash(fmt"Invalid value for {n}, it should be in the range [{min}, {max}]", windowTitle)

proc crashIfBelow[T](n, min: T, windowTitle: string) =
  if n < min:
    crash(fmt"Invalid value for {n}, it should be >= {min}", windowTitle)

proc crashIfAbove[T](n, min: T, windowTitle: string) =
  if n > max:
    crash(fmt"Invalid value for {n}, it should be <= {max}", windowTitle)

proc removeAllBeforeLast(s, sub: string): string =
  result = s
  let i = result.rfind('.')
  if i != -1:
    result = result[i + 1 .. ^1]

proc getSplitsVersionNumber(configTable: TomlValueRef): float =
  result = configTable["splits_version"].getFloat()

## Returns NilTime if it's an empty time
proc parseSavedTime(str: string): float =
  result = if str == EmptyTime: NilTime
           else: str.strToTimeSecs()

template getKeybindings(tb: TomlValueRef, value: untyped): untyped =
  value = Keybindings()

  value.keymap_version = 1.0 # Assume it's v1.0 if no value is found.
  assignFromToml tb, value.keymapVersion, false

  assignFromToml tb, value.redo
  assignFromToml tb, value.reset
  assignFromToml tb, value.split
  assignFromToml tb, value.undo
  assignFromToml tb, value.lockTimer

  # When the skip keybinding feature was added
  if value.keymapVersion >= 2.0:
    assignFromToml tb, value.skipSegment
  else: # Provide a default value for old versions
    value.skipSegment = Keybinding(keyId: KeyCode.None,
                                   requiresShift: false,
                                   requiresCtrl: false)

template getKeybinding(value: untyped, tb, subTb: TomlValueRef): untyped =
  value = Keybinding()
  assignFromToml subTb, value.keyId
  assignFromToml subTb, value.requiresShift
  assignFromToml subTb, value.requiresCtrl

template getMessages(value: untyped, tb, subTb: TomlValueRef): untyped =
  value = Messages()
  assignFromToml subTb, value.lockedMode
  assignFromToml subTb, value.currentPace
  assignFromToml subTb, value.personalBest
  assignFromToml subTb, value.bestPossibleTime
  assignFromToml subTb, value.sumOfBest
  assignFromToml subTb, value.worldRecord
  assignFromToml subTb, value.worldRecordText
  assignFromToml subTb, value.runsThatReachHere
  assignFromToml subTb, value.comparisonPb
  assignFromToml subTb, value.comparisonSob
  assignFromToml subTb, value.me

  let ver = tb["style_version"].getFloat()
  if ver < 7.0:
    value.emptyComparisonText = "* "
  elif ver < 3.0:
    value.skippedSegmentText = "," 
  else:
    assignFromToml subTb, value.skippedSegmentText
    assignFromToml subTb, value.emptyComparisonText

  if ver < 4.0:
      value.loadedSkippedSegmentText = "."
  else:
    assignFromToml subTb, value.loadedSkippedSegmentText

  if ver < 5.0:
    value.bestPaceEver = "Best pace ever?"
    value.bestPaceYes = "Yes"
    value.bestPaceNo = "No"
    value.bestPaceNil = "???"
    value.nilTime = "**:**"

    # Old versions added ':' at runtime, because they weren't made with the
    # possibility of using something else (e.g. '?') in mind
    value.currentPace &= ':'
    value.personalBest &= ':'
    value.bestPossibleTime &= ':'
    value.sumOfBest &= ':'
    value.worldRecord &= ':'
    value.runsThatReachHere &= ':'
  else:
    assignFromToml subTb, value.bestPaceEver
    assignFromToml subTb, value.bestPaceYes
    assignFromToml subTb, value.bestPaceNo
    assignFromToml subTb, value.bestPaceNil
    assignFromToml subTb, value.nilTime

template getColors(value: untyped, tb, subTb: TomlValueRef): untyped =
  let ver = tb["style_version"].getFloat()

  value = Colors()
  assignFromToml subTb, value.background
  assignFromToml subTb, value.detailedTimer
  assignFromToml subTb, value.paceAheadGaining
  assignFromToml subTb, value.paceAheadLosing
  assignFromToml subTb, value.paceBehindGaining
  assignFromToml subTb, value.paceBehindLosing
  assignFromToml subTb, value.paceBest
  assignFromToml subTb, value.separator
  assignFromToml subTb, value.textBase

  # Version that added PB colors
  if ver < 6.0:
    # Old behavior by default
    value.pb = value.paceAheadGaining
  else:
    assignFromToml subTb, value.pb

template getSeqSegment(value: untyped, tb, subTb: TomlValueRef): untyped = 
  const ErrorWindowTitle = "Segments"
  let splitsVer = getSplitsVersionNumber(tb)
  value = @[]
  let arrTbl = subTb.getElems()
  for i, elem in arrTbl:
    var seg = Segment()
    defer: value.add(seg)
    assignFromToml elem, seg.name
    assignFromToml elem, seg.resetCount
    seg.resetCount.crashIfBelow(0, ErrorWindowTitle)

    if splitsVer >= 3.0: # When absolute times started being stored
      assignFromToml elem, seg.pbSplitFinishTime

      seg.initiallySkippedSegment = seg.pbSplitFinishTime == SkippedSegment
      if not seg.initiallySkippedSegment:
        seg.pbCompletedTimeAbs = parseSavedTime(seg.pbSplitFinishTime)

      assignFromToml elem, seg.bestSegmentRelativeTime
      seg.bestSegmentTimeRel = parseSavedTime(seg.bestSegmentRelativeTime)
    else: # Handle old format
      assignFromToml elem, seg.bestSegmentTimeRel
      # Calculate abs PB time
      assignFromToml elem, seg.pbTimeRel
      var absPbTime = seg.pbTimeRel
      for j in countdown(i - 1, 0):
        absPbTime += value[j].pbTimeRel

      seg.pbCompletedTimeAbs = absPbTime

    if splitsVer >= 6.0: # When best entries started being stored
      assignFromToml elem, seg.bestEntryTime
      seg.bestEntryTimeAbs = parseSavedTime(seg.bestEntryTime)
    else:
      seg.bestEntryTimeAbs = NilTime

    # Calculates relative time. Don't ask me why splits.calculatePbRelTime()
    # doesn't work here specifically even though the logic is correct and
    # the proc has no side effects; templates are evil
    var relTime = seg.pbCompletedTimeAbs
    if i > 0:
      relTime -= value[i - 1].pbCompletedTimeAbs
    seg.pbTimeRel = relTime

template getTimerStyle(value: untyped, tb: TomlValueRef): untyped = 
  value = TimerStyle()
  const ErrorWindowTitle = "Timer Style"

  assignFromToml tb, value.styleVersion
  value.styleVersion.crashIfBelow(0.0, ErrorWindowTitle)

  if value.styleVersion > CurrentStyleVersion:
    crash("Can not load style, it was made for a more recent version of jump_dive_clock")

  assignFromToml tb, value.attemptCountFontSize
  value.attemptCountFontSize.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.attemptCountFontSpacing
  value.attemptCountFontSpacing.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.attemptCountTextPosX
  value.attemptCountTextPosX.crashIfOutOfRange(0.0, 1.0, ErrorWindowTitle)

  assignFromToml tb, value.attemptCountTextPosY
  value.attemptCountTextPosY.crashIfOutOfRange(0.0, 1.0, ErrorWindowTitle)

  assignFromToml tb, value.categoryTitleFontSize
  value.categoryTitleFontSize.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.categoryTitleFontSpacing
  value.categoryTitleFontSpacing.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.defaultWindowWidth
  value.defaultWindowWidth.crashIfBelow(1, ErrorWindowTitle)

  assignFromToml tb, value.defaultWindowHeight
  value.defaultWindowHeight.crashIfBelow(1, ErrorWindowTitle)

  assignFromToml tb, value.detailedTimer

  assignFromToml tb, value.detailedTimerSize
  value.detailedTimerSize.crashIfOutOfRange(0.0, 1.0, ErrorWindowTitle)

  assignFromToml tb, value.detailedTimerMarginX
  value.detailedTimerMarginX.crashIfOutOfRange(0.0, 1.0, ErrorWindowTitle)

  assignFromToml tb, value.detailedTimerMarginY
  value.detailedTimerMarginY.crashIfOutOfRange(0.0, 1.0, ErrorWindowTitle)

  assignFromToml tb, value.drawTimerContainerTopSeparator

  assignFromToml tb, value.extraStats

  assignFromToml tb, value.fontFile

  assignFromToml tb, value.gameTitleFontSize
  value.gameTitleFontSize.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.gameTitleFontSpacing
  value.gameTitleFontSpacing.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.headerHeight
  value.headerHeight.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.colors

  assignFromToml tb, value.maxSegmentSize
  value.maxSegmentSize.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.minSegmentsAheadToShow
  value.minSegmentsAheadToShow.crashIfBelow(0, ErrorWindowTitle)

  assignFromToml tb, value.segmentFontSize
  value.segmentFontSize.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.segmentFontSpacing
  value.segmentFontSpacing.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.segmentMargin
  value.segmentMargin.crashIfBelow(0, ErrorWindowTitle)

  assignFromToml tb, value.segmentsPerScreen
  value.segmentsPerScreen.crashIfBelow(0, ErrorWindowTitle)

  assignFromToml tb, value.separatorSize
  value.separatorSize.crashIfBelow(0, ErrorWindowTitle)

  assignFromToml tb, value.timerFontSize
  value.timerFontSize.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.timerFontSpacing
  value.timerFontSpacing.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.messages

  assignFromToml tb, value.timerSize
  value.timerSize.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.titleCategoryGap
  value.titleCategoryGap.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.comparisonFontSize
  value.comparisonFontSize.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.comparisonFontSpacing
  value.comparisonFontSpacing.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.comparisonSpacing
  value.comparisonSpacing.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.compareToSob

  assignFromToml tb, value.comparisonHeaderFontSize
  value.comparisonHeaderFontSize.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.comparisonHeaderFontSpacing
  value.comparisonHeaderFontSpacing.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.comparisonHeaderMarginX
  value.comparisonHeaderMarginX.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.comparisonHeaderMarginY
  value.comparisonHeaderMarginY.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.comparisonHeaderSpacingX
  value.comparisonHeaderSpacingX.crashIfBelow(0.0, ErrorWindowTitle)

  assignFromToml tb, value.detailedComparisons

  if value.styleVersion < 8.0:
    value.alwaysShowLastSegment = false
  else:
    assignFromToml tb, value.alwaysShowLastSegment


template getSplits(value: untyped, tb: TomlValueRef): untyped =
  value = Splits()
  assignFromToml tb, value.attemptCount
  assignFromToml tb, value.gameName
  assignFromToml tb, value.category
  assignFromToml tb, value.completedRunBefore
  assignFromToml tb, value.maximumFramerate
  assignFromToml tb, value.splitsVersion
  assignFromToml tb, value.styleName
  assignFromToml tb, value.keymapName
  assignFromToml tb, value.worldRecordOwner
  assignFromToml tb, value.segments

  # Version that started storing the WR as a string
  if value.splitsVersion < 4.0: 
    value.worldRecordTime = tb[OldWrKey].getFloat()
  else:
    var wrK: string
    # Version that renamed world_record_seconds to world_record_time
    if value.splitsVersion >= 5.0:
      wrK = WrTimeKey
    else:
      wrK = OldWrKey

    let unparsedT = tb[wrK].getStr()
    if unparsedT == EmptyTime:
      value.worldRecordTime = NoWrTime
    else:
      value.worldRecordTime = unparsedT.strToTimeSecs()

  const ErrorWindowTitle = "Splits"
  value.maximumFramerate.crashIfBelow(0, ErrorWindowTitle)
  value.attemptCount.crashIfBelow(0, ErrorWindowTitle)


template getAppConfig(value: untyped, tb: TomlValueRef): untyped =
  let def = getDefaultConfig(CurrentConfigVersion)
  value = AppConfig()
  assignFromToml tb, value.configVersion
  assignFromToml tb, value.maxBackups
  assignFromToml tb, value.windowResizable
  assignFromToml tb, value.hotReloading
  
  # Version that added scaling options
  if value.configVersion < 3.0:
    value.considerHiDpi = def.considerHiDpi
    value.drawScale = def.drawScale
  else:
    assignFromToml tb, value.considerHiDpi
    assignFromToml tb, value.drawScale

  # Version that added file logging
  if value.configVersion < 4.0:
    value.logToFile = def.logToFile
  else:
    assignFromToml tb, value.logToFile

  const ErrorWindowTitle = "General Configuration"
  value.maxBackups.crashIfBelow(0, ErrorWindowTitle)
  value.maxBackups.crashIfBelow(0, ErrorWindowTitle)

template getSeqStatType(value: untyped, tb, subTb: TomlValueRef): untyped = 
  let sts = subTb.getElems()
  value = newSeq[StatType]()
  for st in sts:
    let rawV = st.getStr()
    try:
      value.add(parseEnum[StatType](rawV))
    except ValueError:
      echo("Unknown stat name " & rawV)

  # They are saved in reverse order
  value.reverse()

template getKeyCode(value: untyped, tb, subTb: TomlValueRef): untyped = 
  let code = subTb.getStr()
  try:
    value = parseEnum[KeyCode](code)
  except ValueError:
    crash("Invalid KeyCode " & code)

template assignValuesTo(value: untyped, tb, subTb: TomlValueRef): untyped =
  # Any type with more than one or two lines has its own template
  const FIELD_TYPE_NAME = repr(typeof(value))
  when FIELD_TYPE_NAME == "string":
    value = subTb.getStr()
  elif FIELD_TYPE_NAME == "int":
    value = subTb.getInt()
  elif FIELD_TYPE_NAME == "float":
    value = subTb.getFloat()
  elif FIELD_TYPE_NAME == "bool":
    value = subTb.getBool()
  elif FIELD_TYPE_NAME == repr(Keybindings):
    getKeybindings tb, value
  elif FIELD_TYPE_NAME == repr(Keybinding):
    getKeybinding value, tb, subTb
  elif FIELD_TYPE_NAME == repr(KeyCode):
    getKeyCode value, tb, subTb
  elif FIELD_TYPE_NAME == repr(Messages):
    getMessages value, tb, subTb
  elif FIELD_TYPE_NAME == repr(Colors):
    getColors value, tb, subTb
  elif FIELD_TYPE_NAME == repr(Color):
    let colorStr = subTb.getStr("#ffffff99")
    value = colorStringToColor(colorStr)
  elif FIELD_TYPE_NAME == repr(seq[Segment]):
    getSeqSegment value, tb, subTb
  elif FIELD_TYPE_NAME == repr(TimerStyle):
    getTimerStyle value, tb
  elif FIELD_TYPE_NAME == repr(Splits):
    getSplits value, tb
  elif FIELD_TYPE_NAME == repr(AppConfig):
    getAppConfig value, tb
  elif FIELD_TYPE_NAME == repr(seq[StatType]):
    getSeqStatType value, tb, subTb
  else:
    var msg = "The type "
    msg &= FIELD_TYPE_NAME
    msg &= " is not available for parsing"
    raise newException(ValueError, msg)

template assignFromToml*(tb: TomlValueRef,
                         value: untyped, requiredKey: bool = true): untyped =
  ## tb: the TOML table in question
  ## value: a field (e.g. foo.bar)
  ## a function that will be used to acess the table (e.g. getFloat or getBool)
  ## t: the default value of the table if the acessed value does not exist
  var tomlPath = convertCamelToSnakeCase(astToStr(value))

  tomlPath = tomlPath.removeAllBeforeLast(".")

  var subTb: TomlValueRef
  # Due to template type issues, TomlValueRef.hasKey couldn't be used, so we are
  # doing exception handling instead
  try:
    subTb = tb[tomlPath]
  except KeyError:
    subTb = nil

  const ParseableBaseTypes = [
    repr(typeof(Keybindings)),
    repr(typeof(AppConfig)),
    repr(typeof(TimerStyle)),
    repr(typeof(Splits)),
  ]

  if tb.hasKey(tomlPath) or (not tomlPath.contains('.') and
                         repr(typeof(value)) in ParseableBaseTypes):
    assignValuesTo value, tb, subTb
  elif requiredKey: # Key not found
    echo()
    raise newException(KeyError, "Required configuration key not found: " &
                        convertCamelToSnakeCase(astToStr(
                            value)).removeAllBeforeLast("."))

template parseOrErr(v: TomlValueRef, path: string): untyped =
  try:
    v = parseFile(path)
  except IOError:
    # The r value is a result from functions that return
    # a tuple[v: TYPE, r: Result]
    result.r = Result(success: false,
                      errMsg: fmt"Failed to access the '{path}' file")
    return

proc loadFrom*[T](path: string, log: bool = true): tuple[v: T, r: Result] =
  if log:
    echo(fmt"Attempting to load '{path}'")

  var
    obj: T
    err = Result(success: true)

  var tomlTable: TomlValueRef
  parseOrErr tomlTable, path

  assignFromToml tomlTable, obj
  result = (obj, err)

