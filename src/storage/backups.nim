#    jump_dive_clock - Speedrun timer for Linux.
#    Copyright (C) 2023-2025  Miguel Nornkirn
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import std/[strformat, strutils, os]
import ../utils

var
  backupFolder = ""
  maxBackupFiles = -1
  backupFileName = "" # e.g. "file_name_$1.toml"

## Returns -1 on fail
proc parseFileNumber(filename: string): int =
  let tokens = filename.split('_')
  try:
    result = parseInt(tokens[tokens.len() - 1].split(".toml")[0])
  except ValueError:
    result = -1

proc setupBackups*(path: string, maxBackupCount: int, fileName: string) =
  backupFolder = path
  maxBackupFiles = maxBackupCount
  backupFileName = fileName

## Returns -1 on err
proc getLatestBackupNumber(dirToWalk: string): int =
  var largestN = -1
  for kind, path in walkDir(dirToWalk):
    if kind != pcFile:
      continue

    let
      filename = extractFilename(path)
      n = parseFileNumber(filename)

    if n < 0:
      continue

    if n > largestN:
      largestN = n

  result = largestN

proc purgeOldBackups(newBackupN: int, dirToWalk: string) =
  for kind, path in walkDir(dirToWalk):
    if kind != pcFile:
      continue

    let filename = extractFilename(path)
    if diff(newBackupN, parseFileNumber(filename)) > maxBackupFiles:
      try:
        removeFile(path)
      except OSError:
        continue

proc backupOldSplits*(data: string) =
  createDir(backupFolder)

  let
    filePathPrefix = fmt"{backupFolder}/{backupFileName}_"
    backupFilesDirWalkStr = fmt"{backupFolder}/"
    lastBackupN = getLatestBackupNumber(backupFilesDirWalkStr)
    lastBackup = if lastBackupN == -1: ""
                 else: readFile(fmt"{filePathPrefix}{lastBackupN}.toml")
  var newBackupN = lastBackupN + 1

  if lastBackup != data:
    writeFile(fmt"{filePathPrefix}{newBackupN}.toml", data)
    purgeOldBackups(newBackupN, backupFilesDirWalkStr)

