# Documentation

## CLI usage

```text
jump_dive_clock [config_folder:<folder>] split:<split name>

config_folder: the folder where config.yml is, ~/.config by default.
split: the name of the split you want to use, it is stored in the config_folder.

The parts surrounded by [] are optional.
The parts surrounded by <> should be replaced with the appropriate text.

Examples:
jump_dive_clock split:cool_game
jump_dive_clock config_folder:$HOME/speedruns/ split:example
```

## Wayland

### Installation

Wayland usage requires the following packages to be installed (at least on Fedora):
```
wayland-devel libxkbcommon-devel mesa-libGL-devel
```
For commands such as `build` and `install`, you need to add `-wayland` so it
works (i.e. `make install-wayland` instead of `make install`, or
`make debug-run-wayland` instead of `make debug-run`).

Please open any issues you might find while running on Wayland, it hasn't been
tested there (I use X11).

### Issues

HiDPI displays don't seem to work correctly at least on some setups.

## Permissions

Reading global keybindings requires read access to the contents of /dev/input/.
For this reason, the user that runs jump_dive_clock needs to be in the "input"
group. To add your user to the input group, run:
```bash
sudo gpasswd -a your_user_name input
```

You might need to log-out then log-in again.

## Global and local keybindings

Locking the timer only works if the window is focused, unlike any other
commands.

Thus, timer locking uses a local keybinding and other commands use global
keybindings.

## Creating splits for a new game

Copy the example split, change `completed_run_before` to `false` and replace
the example segments with your own segments. If you never completed a segment
before, set `pb_split_finish_time` and `best_segment_relative_time` to `"None"`.

If you are confused, read the rest of this documentation or open an issue to ask
a question.

## Positions

Positions are always relative to whatever contains the object in question
(e.g. a piece of text is contained inside a segment).

X begins (i.e. is 0) at the leftmost part of the container and ends (i.e. is 1)
at the rightmost part.

Y begins at the top of the container and ends on its bottom.

For example, (0.5x, 0.5y) centers something the middle of the container.

## Configuration files

Using this timer requires manually setting up three different configuration
files. These being the [splits](splits/example.toml) file, the
[style](styles/example.toml) file and the [keymaps](keymaps/example.toml) file.

A config.toml is also required, but it is automatically generated if it does
not exist.

Using the example files is recommended as a starting point for your own
configurations.

All configuration files are in [TOML](https://toml.io/en/). The syntax is
simple to figure out after reading the examples.

### config.toml

#### config_version

Do not change manually.

#### max_backups

The maximum amount of backups that can exist before old ones start to be
deleted. A larger amount will you use more disk space.

#### window_resizable

If you are allowed to resize the window after the timer opens or not.

#### hot_reloading

Constantly reloads the styles file. This is for immediately seeing changes made
to the styles file when testing different values. This should be disabled when
during speedrunning because it will have a poorer performance/use more
resources of the computer without a good reason.

#### consider_high_dpi

Whether the timer should consider HiDPI when drawing the screen. This should be
set to `true` unless you know what you are doing or are having scaling-related issues.

#### draw_scale

At which scale should the timer draw things (e.g. 1.0, 1.5, 2.0, etc). Should
be set to 1.0 unless you have a good reason to change it.

### Keymaps

File that decides which keys map to each actions.

#### {action name}.key_id

The key code that you want to correspond to the action. You can find a list
of keycodes in [keycodes.nim](src/keycodes.nim).

#### {action name}.requires_shift
If the action requires the shift key to be pressed for it to be executed.

#### {action name}.requires_ctrl
If the action requires the ctrl key to be pressed for it to be executed.

#### keyboard_id (deprecated)

Don't use this if your keymap version is 7.0 or later. This is in the
documentation for historical purposes.

The path to the keyboard that will be used for controlling the timer. You can
easily find the name of your device by searching for it in the 
/dev/input/by-id/ folder. See the tutorial at the beggining of this file.

Newer versions of the timer ignore this option and remove it from keymap files.

### Styles

Styles define how splits look. You can reuse the same style for many splits.

#### detailed_comparisons

Shows miliseconds in the time comparisons.

Type: boolean (true or false).

#### always_show_last_segment

Whether to always keep the last segment of the run on the screen, even if it
would otherwise not be visible due to scrolling.

Type: boolean (true or false).

#### {object name}_font_size

How large is the object's font.

Type: integer (must be greater than 0).

#### {object name}_font_spacing

How large is the spacing of the objects's font.

Type: integer (must be greater than 0).

#### {object name}_text_pos_x

The relative horizontal position of the object.

Type: number (range: 0.0 to 1.0).

#### {object name}_size_text_pos_y

The relative vertical position of the object.

Type: number (range: 0.0 to 1.0).

#### default_window_height

The default window height.

Type: integer (must be greater than 0).

#### default_window_width

The default window width.

Type: integer (must be greater than 0).

#### detailed_timer

If the detailed timer is enabled or not.

Type: boolean (true or false).

#### detailed_timer_size

How large is the detailed timer's size, relative to the main timer's size.

Type: number (range: 0.0 to 1.0).

#### detailed_timer_margin_x

The size of the margin at the left of the detailed timer, relative to the
window's width.

Type: number (range: 0.0 to 1.0).

#### detailed_timer_margin_y

The size of the margin at the top of the detailed timer, relative to the
window's height.

Type: number (range: 0.0 to 1.0).

#### draw_timer_container_top_separator

If you should draw the separator at the top of the timer's container.

Type: boolean (true or false).

#### extra_stats

List of extra stats to show in order.

Type: list of extra stats.

Valid extra stats: CurrentPace, PersonalBest, BestPossibleTime, SumOfBest,
WorldRecord, RunsThatReachHere, BestPaceEver.

##### CurrentPace

Shows a prediction of the final time of the run, based on the current pace and
assuming that the runner will play the rest of the run as well as in their PB.

##### PersonalBest

Shows the best time that the runner ever got.

##### BestPossibleTime

Shows a prediction of the final time of the run, based on the combination of
the segments that are already finished and the runner's best segments.

##### SumOfBest

Shows the theoretically best run possible, based on the runner's best segments.

##### WorldRecord

Shows the world record and its owner.

##### RunsThatReachHere

Shows the percentage of runs that reach the current segment of the run.

##### BestPaceEver

Whether the current run is the best pace ever.

#### font_file

The name of the font file that should be used (located in the config folder's fonts
directory).

Type: text.

#### header_height

#### colors

The colors of every part of the timer, in the hexadecimal format.

The hexadecimal format is "#rrggbbaa". Use a hex color picker to find the color
codes, you can search for an online one.

Type: text.

Colors:
* colors.background;
* colors.detailed_timer;
* colors.pace_ahead_gaining;
* colors.pace_ahead_losing;
* colors.behind_gaining;
* colors.behind_losing.

#### max_segment_size

Maximum size (in pixels) of each segment.

Type: integer (must be greater than 0).

#### min_segments_ahead_to_show

The minimum amount of uncompleted segments to show (only relevant if you have  more
segments than the amount that can see per screen at once).

Type: integer (must be greater than 0).

#### segment_margin

The distance of the segment from the rightmost part of the screen in pixels.

Type: integer (must be greater than 0).

#### segments_per_screen

The maximum number of segments that can be seen per screen at once.

Type: integer (must be greater than 0).

#### separator_size

How big is line that divides segments.

Type: integer.

#### style_version

Used for upgrading the config, do not change unless you know what you are doing.

#### timer_size

How big the timer is.

Type: integer (must be greater than 0).

#### title_category_gap

The size of the gap between the title of the game and the category.

#### compare_to_sob

If, in addition to comparing to PB, the timer should compare the current time
to the sum of best times (i.e. the theoretically best possible run).

Type: boolean (true or false).

#### comparison_spacing

The spacing between comparisons (against PB and SoB) aand the segment time.

Type: integer (must be greater than 0).

#### comparison_header_margin_x

How far, in pixels, the title of the comparisons should be from the right of the
header.

Type: integer (must be greater than 0).

#### comparison_header_margin_y

How far above, in pixels, the title of the comparisons should be in relation to
the bottom of the header.

Type: integer (must be greater than 0).

#### comparison_header_spacing_x

The amount of space, in pixels, that should be between the PB and SoB
comparisons.

Type: integer (must be greater than 0).

#### Messages

The specific strings of text that should be displayed in certain
places/situations. These are very useful for those who do not like
the default strings or that desire to translate them to different
languages.

##### locked_mode

The message that shows in place of the timer when the timer is locked.

Type: text.

##### current_pace

The message that shows as the CurrentPace statistic's name.

Type: text.

##### personal_best

The message that shows as the PersonalBest statistic's name.

Type: text.

##### best_possible_time

The message that shows as the BestPossibleTime statistic's name.

Type: text.

##### sum_of_best

The message that shows as the SumOfBest statistic's name.

Type: text.

##### world_record

The message that shows as the WorldRecord statistic's name.

Type: text.

##### world_record_text

It is a piece of text displaying the time of the world record owner. In the 
configuration file, it must include "$1" and "$2". At runtime, "$1" will be
replaced with the time and $2 will be replaced by the owner of the world record.

```
Example: "$1 by $2" is rendered as "22:23 by John Doe" (time: 22:23, player: John Doe).
Another example: "$2の$1" is rendered as "山田の22:21" (time: 22:21, player: 山田).
```

The possibility of using $1 and $2 in any order is allowed so it is possible
to properly translate the world record message to languages that have a 
different word order than English.

Type: text.

##### runs_that_reach_here

The message that shows as the RunsThatReachHere statistic's name.

Type: text.

##### comparison_pb

The message that appears as the title of the comparison to the PB.

Type: text.

##### comparison_sob

The message that appears as the title of the comparison to the SoB.

Type: text.

##### me

The word for "me" that appears when you beat the world record. Replacing this
word is only important when translating to different languages.

```
Example: you beat the word record with the time of 1:24 and the WR message
changes to "1:24 by me".
```

Type: text.

##### skipped_segment_text

What appears in the place of the time for the sement when you skip a segment.

Type: text.

##### loaded_skipped_segment_text

What appears in the place of the time for the sement when you comparing to
splits with skipped segments.

Type: text.

##### empty_comparison_text

What appears in the place of the comparison to a completed segment if it got
skipped or it is being completed for the first time.

Formerly named new_segment_text.

Type: text.

##### best_pace_ever

The message that shows as the BestPaceEver statistic's name.

Type: text.

##### best_pace_ever_yes

The text to show if it's the best pace ever.

Type: text.

##### best_pace_ever_no

The text to show if it's not the best pace ever.

Type: text.

##### best_pace_ever_nil

The text to show if it couldn't be determined whether it's the best pace ever.

Type: text.

### Splits

Splits define how your splits will behave and will store their data.

#### attempt_count

How many attempts were made, not recommended to change manually.

Type: integer (must be greater than 0).

#### category

The name of the category that is being ran.

Type: text.

#### completed_run_before

If any run has ever been completed before. Not recommended to be changed
manually unless you know what you are doing.

Type: boolean (true or false).

#### game_name

The title of the game.

Type: text.

#### maximum_framerate

The maximum framerate of the timer. More increases fluidity and decreases the chances
of an input being dropped if pressed too quickly, with the caveat of increasing the
resource usage.

Recommended value: the framerate of the recording/stream or at least 30.

Type: integer (must be greater than 0).

#### segments

List of segments.

Each segment must have the following keys.

##### best_segment_relative_time

"Gold" time, that is, the best time the speedrunner even got in this segment.
Relative to the beggining of the segment.

Type: time string (e.g. "1:23:45.678", quotes included, hours and minutes are
optional).

##### name

The name of the segment.

Type: text.

##### pb_split_finish_time

The time that the runner got in this segment on their fastest completed run.
It is relative to the beggining of the run.

Type: time string (e.g. "1:23:45.678", quotes included, hours and minutes are
optional).

##### reset_count

The amount of time that the player resetted in this segment specifically. Do
not change this manually without changing the attempt count. The sum of the
attempts of all segments should be equal to attempt_count.

Type: number.

#### splits_version

Used for upgrades, DO NOT CHANGE.

Type: number.

#### style_name

The name of the style file.

#### keymap_name

The name of the keymap file.

#### world_record_owner

The name of the world record's owner.

Type: text.

#### world_record_seconds

The time of the world record.

Type: time string (e.g. "1:23:45.678", quotes included, hours and minutes are
optional).
