# Package
version       = "1.0.1"
author        = "Miguel Nornkirn"
description   = "Speedrun timer for Linux written in Nim."
license       = "GPL-3.0-or-later"
srcDir        = "src"
bin           = @["jump_dive_clock"]

# Dependencies
requires "nim >= 2.0.0"
requires "naylib == 5.1.2"
requires "parsetoml == 0.7.1"
requires "tinydialogs == 1.1.0"

