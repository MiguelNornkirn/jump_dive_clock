# jump_dive_clock

A speedrun timer for Linux. Intended for my personal use. I add features that
seem useful to me. There aren't many timers for Linux so I suppose that it
could be useful for somebody else. Made with fellow advanced users in mind.

## Installation

## FAQ

### Why did you create this?

I tried some timers for Linux and I either disliked them or they didn't work at all. So I made my
own. And made this to share to other people that may find it useful.

### What features do you have?

- [x] Running on Linux
- [x] Marking time (with an optional segment-specific timer), splitting,
    undoing, redoing and skipping segments
- [x] Comparing the current time with your personal best and/or sum of best
- [x] High customizability via human-readable TOML files
- [x] Having stats (best possible time, current pace, percentage of runs that reach
    each segment, personal best, world record, sum of best, best pace ever)
- [x] Having a backup system that creates copies of the speedgame's data whenever a change is made to
    it
- [x] Locking the timer, preventing inputs from being triggered globally (useful if you want to do
    something else while having your speedrun timer open)

### Should I use this?

The documentation should be complete and the timer is stable by this point (i.e.
old configurations are unlikely to break after updates). But you shouldn't
expect perfect polish, this is an one-man project made by some guy who wanted
a working speedrun timer.

It's very usable, it's software that I actually use for almost every single one
of my speedruns.

### How do I create splits for a new game? How do I setup this?

Read the [documentation](DOCS.md). Besides installation instructions, it contains
everything that you need to know.

### Doing the previous thing and compiling from source are too hard!

Then you're probably not the intended audience of this program. You're expected
to read instructions and edit config files manually.

### Why is it not working on my machine? It says "Permission denied"

This app requires the current user to be in the `input` group or be ran as
root. Since the latter is a generally a terrible idea, you should do the former.

This is as simple as running
```bash
sudo gpasswd -a your_user_name input
```

You might need to log-out then log-in again.

### Where did this name come from?

I speedrun a Mario fangame where the fastest way to move without equipment is
to repeatedly jump and dive midair.

### Other OSes?

I don't intend on doing ports for other OSes since this was made exclusively
with the purpose of being ran on a Linux machine, but I'm open to pull requests.
I may make a port in the future if I ever move to a different operating system.

### Who uses this?

As far as I'm aware, only me. You can see me using it in my
[speedrun profile](https://www.speedrun.com/users/SoicBR).

## Installing

### From source

If you run into any issues while trying to build from source, feel free to open
an issue asking for help.

#### Installing the dependencies

This program requires Nim, you can install it [here](https://nim-lang.org/install_unix.html)
or from your distribution's package manager, although using the website is
the recommended approach since your package manager's version of Nim could
be too outdated to compile jump_dive_clock.

You also need the following dependencies:
* libx11;
* libxcursor;
* libxrandr;
* libxinerama;
* libxtst;
* libgl.

To install them, run:
```bash
# On Debian/Ubuntu based systems
sudo apt install libx11-dev libxcursor-dev libxrandr-dev libxinerama-dev libxtst-dev libgl-dev
```

#### Compiling

This assumes that you've already installed all of the necessary dependencies
(instructions above).

Navigate to the folder where you want to install `jump_dive_clock` using `cd`.
And then, run the following commands:
```sh
git clone https://gitlab.com/MiguelNornkirn/jump_dive_clock.git
cd jump_dive_clock
make install
# You should *NOT* run make as root
```
Note: if you're on Wayland, you need to use `make install-wayland` instead.

It should build without errors. If something goes wrong, try figuring it out
by reading the error messages, the instructions or opening an issue (in that
order).

Then, you can run the application in the terminal with the `jump_dive_clock` command, assuming
that the ~/.local/bin/ folder is in your $PATH. In case it is not, run:
```sh
echo export PATH=$HOME/.local/bin/:\$PATH >> ~/.bashrc
```

Then, restart your terminal and try running `jump_dive_clock` again. Alternatively, you
can run the following command:
```sh
# That should reload your $PATH without needing to restart the terminal
source ~/.bashrc
```

If you do not plan to use untrusted configuration files from other users that
may try to exploit hypothetical security vulnerabilities in this program and are not afraid
of more weirder behavior in cases of bugs, you can build the
program in danger mode for marginally better performance. To do this, instead of
running `make install`, use `make install-danger`. This removes runtime checks
from the code without changing funcionality. The equivalent command for Wayland
is `make install-danger-wayland`.

## Uninstalling
Run:
```sh
cd folder_where_you_cloned_this_repo/jump_dive_clock
make uninstall
```

## Usage

This program is intended for users who know how to launch applications from
the command line, don't mind reading [documentation](DOCS.md) and can manually
edit config files.

If you have any questions, feel free to open an issue.

## Modification and redistribution

You are free to modify and/or redistribute copies of this software, as long as
you share the source code and keep it under the same (or a compatible) license.

For details, see [License](LICENSE).
